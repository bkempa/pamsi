import random


def merge_sort(collection, start_index, end_index):
    if start_index < end_index:
        middle = start_index + (end_index - start_index) // 2
        merge_sort(collection, start_index, middle)
        merge_sort(collection, middle + 1, end_index)
        merge(collection, start_index, middle, end_index)


def merge(collection, start_index, middle, end_index):
    left_size = middle - start_index + 1
    right_size = end_index - middle
    left_list = []
    right_list = []
    for i in range(left_size):
        left_list.append(collection[start_index + i])

    for i in range(right_size):
        right_list.append(collection[middle + i + 1])

    i = j = 0
    k = start_index

    # merging temp lists
    while i < left_size and j < right_size:
        if left_list[i] <= right_list[j]:
            collection[k] = left_list[i]
            i += 1
        else:
            collection[k] = right_list[j]
            j += 1
        k += 1

    # remaining elements in temp lists
    while i < left_size:
        collection[k] = left_list[i]
        i += 1
        k += 1

    while j < right_size:
        collection[k] = right_list[j]
        j += 1
        k += 1


def quicksort(collection, start_index, end_index):
    if start_index < end_index:
        ind = random_pivot(collection, start_index, end_index)
        quicksort(collection, start_index, ind)
        quicksort(collection, ind + 1, end_index)


def partition(collection, ind1, ind2):
    pivot = int(float(collection[ind1].rank))
    i = ind1 - 1
    j = ind2 + 1

    while True:
        while True:
            i += 1
            if collection[i].rank >= collection[pivot].rank:
                break
        while True:
            j -= 1
            if collection[j].rank <= collection[pivot].rank:
                break
        if i >= j:
            return j
        collection[i], collection[j] = collection[j], collection[i]


def random_pivot(collection, start_index, end_index):
    random_index = 6
    collection[random_index], collection[start_index] = collection[start_index], collection[random_index]
    return partition(collection, start_index, end_index)


def bucket_sort(collection, start_index, end_index):
    buckets = [[] for _ in range(10)]
    collection = collection[start_index:end_index+1]
    for film in collection:
        buckets[int(float(film.rank)) - 1].append(film)

    k = 0
    for i in range(len(buckets)):
        for j in range(len(buckets[i])):
            collection[k] = buckets[i][j]
            k += 1


def check_sort(collection):
    return all(collection[i] <= collection[i + 1] for i in range(len(collection) - 1))
