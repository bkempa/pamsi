from functools import total_ordering


class Film:
    def __init__(self, title, rank):
        self.title = title
        self.rank = rank

    @total_ordering
    def __lt__(self, other):
        return float(self.rank) < float(other.rank)

    @total_ordering
    def __eq__(self, other):
        return float(self.rank) == float(other.rank)

    @total_ordering
    def __le__(self, other):
        return float(self.rank) <= float(other.rank)

    @total_ordering
    def __gt__(self, other):
        return float(self.rank) > float(other.rank)

    def __repr__(self):
        return self.title + "\t(rating: " + str(self.rank) + ")"
