import csv
import time
import film
import sort


def main():
    film_base = parse_csv('projekt2_dane.csv')

    print("Filtering film base...")
    start = time.time()
    filtered_base = filter_list(film_base)
    end = time.time()
    print("Base filtered! Elapsed time: {:.6f} s".format((end - start)))
    print()

    test_sets = create_test_sets(filtered_base)
    sort_functions = [sort.merge_sort, sort.bucket_sort, sort.quicksort]
    for sort_algorithm in sort_functions:
        print("\t\t" + sort_algorithm.__name__.upper().replace('_', ' '))
        for test_set in test_sets:
            sort_list(sort_algorithm, test_set)
        print()
        print()


def parse_csv(filename):
    with open(filename, 'r', encoding="utf-8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
        next(csv_reader)
        file_lines = []
        for row in csv_reader:
            file_lines.append(row)
    return file_lines


def filter_list(film_base):
    filtered_list = [line for line in film_base if line[2] != ""]
    return filtered_list


def convert_list_to_films(film_base):
    films_list = [film.Film(line[1], line[2]) for line in film_base]
    return films_list


def mean_rank(films_list):
    rank_sum = 0
    for curr_film in films_list:
        rank_sum += int(float(curr_film.rank))
    return float(rank_sum) / len(films_list)


def median_rank(films_list):
    if len(films_list) % 2 == 0:
        return (float(films_list[len(films_list) // 2].rank) + float(films_list[len(films_list) // 2 - 1].rank)) / 2
    else:
        return films_list[len(films_list) // 2].rank


def create_test_sets(orig_films_list):
    test_list = []
    film_base_max = convert_list_to_films(orig_films_list)
    film_base_10000 = film_base_max[:10000]
    film_base_100000 = film_base_max[:100000]
    film_base_500000 = film_base_max[:500000]
    test_list.append(film_base_10000)
    test_list.append(film_base_100000)
    test_list.append(film_base_500000)
    test_list.append(film_base_max)
    return test_list


def sort_list(algorithm, list_to_sort):
    print("Sorting " + str(len(list_to_sort)) + " films")
    start = time.time()
    algorithm(list_to_sort, 0, len(list_to_sort) - 1)
    end = time.time()
    print("Elapsed time: {:.6f} s".format((end - start)))
    print("Sorted correctly? " + str(sort.check_sort(list_to_sort)))
    print("Ranking mean value: {:.2f}".format(mean_rank(list_to_sort)))
    print("Ranking median value: " + str(median_rank(list_to_sort)))
    print()


if __name__ == '__main__':
    main()
