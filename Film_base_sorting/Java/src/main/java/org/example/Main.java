package org.example;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        List<String[]> filmList = parseCSV("src/main/resources/projekt2_dane.csv");

        System.out.println("Filtering data base...");
        long start = System.nanoTime();
        filterList(filmList);
        long end = System.nanoTime();
        System.out.printf("Elapsed time: %.6f s.\n", (end - start) * 0.000000001);


        List<List<Film>> testLists1 = createTestSets(filmList);
        List<List<Film>> testLists2 = createTestSets(filmList);
        List<List<Film>> testLists3 = createTestSets(filmList);


        System.out.print("\n************\nBucket sort\n************\n");
        for (List<Film> films : testLists1) {
            System.out.println("\nSorting " + films.size() + " films.");
            start = System.nanoTime();
            Sort.bucketSort(films);
            end = System.nanoTime();
            System.out.printf("Elapsed time: %.6f s.\n", (end - start) * 0.000000001);
            System.out.printf("Ranking mean value: %.2f\n", meanRank(films));
            System.out.println("Ranking median value: " + medianRank(films));
            System.out.println("Sorted correctly: " + Sort.checkSort(films));
        }

        System.out.print("\n************\nMerge sort\n************\n");
        for (List<Film> films : testLists2) {
            System.out.println("\nSorting " + films.size() + " films.");
            start = System.nanoTime();
            Sort.mergeSort(films, 0, films.size() - 1);
            end = System.nanoTime();
            System.out.printf("Elapsed time: %.6f s.\n", (end - start) * 0.000000001);
            System.out.printf("Ranking mean value: %.2f\n", meanRank(films));
            System.out.println("Ranking median value: " + medianRank(films));
            System.out.println("Sorted correctly: " + Sort.checkSort(films));
        }

        System.out.print("\n************\nQuicksort\n************\n");
        for (List<Film> films : testLists3) {
            System.out.println("\nSorting " + films.size() + " films.");
            start = System.nanoTime();
            Sort.quicksort(films, 0, films.size() - 1);
            end = System.nanoTime();
            System.out.printf("Elapsed time: %.6f s.\n", (end - start) * 0.000000001);
            System.out.printf("Ranking mean value: %.2f\n", meanRank(films));
            System.out.println("Ranking median value: " + medianRank(films));
            System.out.println("Sorted correctly: " + Sort.checkSort(films));
        }
    }

    public static List<String[]> parseCSV(String fileName) {
        List<String[]> data = null;
        try {
            FileReader fileReader = new FileReader(fileName);
            CSVReader csvReader = new CSVReaderBuilder(fileReader).withSkipLines(1).build();
            data = csvReader.readAll();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | CsvException e) {
            throw new RuntimeException(e);
        }
        return data;
    }

    public static void filterList(List<String[]> filmList) {
        filmList.removeIf(str -> Objects.equals(str[2], ""));
    }

    public static List<Film> convertListToFilms(List<String[]> filmList) {
        List<Film> filmsArray = new ArrayList<>();
        for (String[] lines : filmList)
            filmsArray.add(new Film(lines[1], Double.valueOf(lines[2]).intValue()));
        return filmsArray;
    }

    public static double meanRank(List<Film> filmList) {
        int sum = 0;
        double mean;
        for (Film film : filmList)
            sum += film.getRating();
        mean = (double) sum / (double) filmList.size();
        return mean;
    }

    public static double medianRank(List<Film> filmList) {
        if (filmList.size() % 2 == 0)
            return (filmList.get(filmList.size()/2).getRating() + filmList.get(filmList.size()/2 - 1).getRating())/2.0;
        else
            return filmList.get(filmList.size()/2).getRating();
    }

    public static List<List<Film>> createTestSets(List<String[]> filmList) {
        List<List<Film>> testList = new ArrayList<>(4);
        List<Film> filmBaseMax = convertListToFilms(filmList);
        List<Film> filmBase10000 = filmBaseMax.subList(0, 10000);
        List<Film> filmBase100000 = filmBaseMax.subList(0, 100000);
        List<Film> filmBase500000 = filmBaseMax.subList(0, 500000);
        testList.add(filmBase10000);
        testList.add(filmBase100000);
        testList.add(filmBase500000);
        testList.add(filmBaseMax);
        return testList;
    }
}