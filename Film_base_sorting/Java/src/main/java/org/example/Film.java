package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Setter
@Getter
@AllArgsConstructor
public class Film extends ArrayList<Film> implements Comparable<Film> {
    private String title;
    private int rating;

    @Override
    public String toString() {
        return this.getTitle() + "\t(rating: " + this.getRating() + ")";
    }

    @Override
    public int compareTo(Film o) {
        return Integer.compare(this.getRating(), o.getRating());
    }
}