package org.example;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@NoArgsConstructor
public final class Sort {
    public static void mergeSort(List<Film> collection, final int startIndex, final int endIndex) {
        if (startIndex < endIndex && startIndex >= 0 && endIndex < collection.size()) {
            int middle = startIndex + (endIndex - startIndex) / 2;
            mergeSort(collection, startIndex, middle);
            mergeSort(collection, middle + 1, endIndex);
            merge(collection, startIndex, middle, endIndex);
        }
        else if (startIndex < 0)
            throw new ArrayIndexOutOfBoundsException("MergeSort: start index cannot be a negative number");
        else if (endIndex >= collection.size())
            throw new ArrayIndexOutOfBoundsException("MergeSort: end index is out of bounds");
    }

    public static void merge(List<Film> collection, final int startIndex, final int middle, final int endIndex) {
        if (startIndex >= 0 && endIndex < collection.size() && middle >= startIndex && middle < endIndex) {
            int i, j, k, leftSize, rightSize;
            leftSize = middle - startIndex + 1;
            rightSize = endIndex - middle;
            Film[] leftArray = new Film[leftSize];
            Film[] rightArray = new Film[rightSize];
            for (i = 0; i < leftSize; ++i)
                leftArray[i] = collection.get(startIndex + i);

            for (j = 0; j < rightSize; ++j)
                rightArray[j] = collection.get(middle + j + 1);

            i = j = 0;
            k = startIndex;

            //merging temp arrays
            while (i < leftSize && j < rightSize) {
                if (!(leftArray[i].getRating() > rightArray[j].getRating())) {
                    collection.set(k, leftArray[i]);
                    i++;
                } else {
                    collection.set(k, rightArray[j]);
                    j++;
                }
                k++;
            }

            //remaining elements in temp arrays
            while (i < leftSize) {
                collection.set(k, leftArray[i]);
                i++;
                k++;
            }

            while (j < rightSize) {
                collection.set(k, rightArray[j]);
                j++;
                k++;
            }
        }
        else if (startIndex < 0)
            throw new ArrayIndexOutOfBoundsException("Merge: starting index cannot be a negative number");
        else if (endIndex >= collection.size())
            throw new ArrayIndexOutOfBoundsException("Merge: end index is out of collection range");
        else
            throw new IllegalArgumentException("Merge: middle index has to be in [startIndex, endIndex) range");
    }

    public static void quicksort(List<Film> collection, final int startIndex, final int endIndex) {
        if (startIndex < endIndex && startIndex >= 0 && endIndex < collection.size()) {
            int ind = randomPivot(collection, startIndex, endIndex);
            quicksort(collection, startIndex, ind);
            quicksort(collection, ind + 1, endIndex);
        }
        else if (endIndex >= collection.size())
            throw new ArrayIndexOutOfBoundsException("Quicksort: end index is out of bounds");
    }

    public static int partition(List<Film> collection, final int ind1, final int ind2) {
        if (ind1 < ind2 && ind1 >= 0 && ind2 < collection.size()) {
            int pivot = collection.get(ind1).getRating();
            int i = ind1 - 1;
            int j = ind2 + 1;

            while (true) {
                do {
                    i++;
                } while (collection.get(i).getRating() < pivot);

                do {
                    j--;
                } while (collection.get(j).getRating() > pivot);

                if (i >= j)
                    return j;

                Collections.swap(collection, i, j);
            }
        }
        else if (ind1 >= ind2)
            throw new IllegalArgumentException("Partition: starting index has to be less than end index");
        else if (ind2 >= collection.size())
            throw new ArrayIndexOutOfBoundsException("Partition: end index is out of bounds");
        else
            throw new ArrayIndexOutOfBoundsException("Partition: starting index has to be a positive number");
    }

    public static int randomPivot(List<Film> collection, final int startIndex, final int endIndex) {
        Random random = new Random();
        int randomIndex = random.nextInt(startIndex, endIndex);
        Collections.swap(collection, randomIndex, endIndex);
        return partition(collection, startIndex, endIndex);
    }

    public static void bucketSort(List<Film> collection) {
        List<List<Film>> bucket = new ArrayList<>();
        for (int i = 0; i < 10; i++)
            bucket.add(i, new ArrayList<>());

        for (Film film : collection)
            bucket.get(film.getRating() - 1).add(film);

        for (int i = 0; i < collection.size(); i++)
            for (int j = 0; j < 10; j++)
                for (int k = 0; k < bucket.get(j).size(); k++) {
                    collection.set(i, bucket.get(j).get(k));
                    i++;
                }
    }

    public static boolean checkSort(List<Film> collection) {
        return IntStream.range(1, collection.size()).noneMatch(i -> collection.get(i - 1).compareTo(collection.get(i)) > 0);
    }

}

