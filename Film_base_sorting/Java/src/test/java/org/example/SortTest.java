package org.example;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SortTest {

    @Test
    void mergeSort() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        Sort.mergeSort(filmsList, 0, filmsList.size() - 1);
        assertTrue(Sort.checkSort(filmsList));
    }

    @Test
    void mergeSortNegativeStartIndex() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> Sort.mergeSort(filmsList, -1, filmsList.size() - 1));
    }

    @Test
    void mergeSortEndIndexOutOfBounds() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> Sort.mergeSort(filmsList, 0, filmsList.size()));
    }

    @Test
    void merge() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        List<Film> filmsList = new ArrayList<>(2);
        filmsList.add(film3);
        filmsList.add(film1);
        Sort.merge(filmsList, 0, 0, filmsList.size() - 1);
        assertTrue(Sort.checkSort(filmsList));
    }

    @Test
    void mergeNegativeStartIndex() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        List<Film> filmsList = new ArrayList<>(2);
        filmsList.add(film3);
        filmsList.add(film1);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> Sort.merge(filmsList, -1, 0, filmsList.size() - 1));
    }

    @Test
    void mergeEndIndexGreaterThanCollectionSize() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        List<Film> filmsList = new ArrayList<>(2);
        filmsList.add(film3);
        filmsList.add(film1);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> Sort.merge(filmsList, 0, 0, filmsList.size()));
    }

    @Test
    void mergeWrongMiddleIndex() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        List<Film> filmsList = new ArrayList<>(2);
        filmsList.add(film3);
        filmsList.add(film1);
        Sort.merge(filmsList, 0, 0, filmsList.size() - 1);
        assertThrows(IllegalArgumentException.class, () -> Sort.merge(filmsList, 0, 1, filmsList.size() - 1));
    }

    @Test
    void mergeNegativeMiddleIndex() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        List<Film> filmsList = new ArrayList<>(2);
        filmsList.add(film3);
        filmsList.add(film1);
        Sort.merge(filmsList, 0, 0, filmsList.size() - 1);
        assertThrows(IllegalArgumentException.class, () -> Sort.merge(filmsList, 0, -1, filmsList.size() - 1));
    }

    @Test
    void quicksort() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        Sort.quicksort(filmsList, 0, filmsList.size() - 1);
        assertTrue(Sort.checkSort(filmsList));
    }

    @Test
    void quicksortWrongEndIndex() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> Sort.quicksort(filmsList, 0, 5));
    }

    @Test
    void partition() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        assertEquals(2, Sort.partition(filmsList, 0, filmsList.size() - 1));
    }

    @Test
    void partitionWrongEndIndex() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> Sort.partition(filmsList, 0, 5));
    }

    @Test
    void partitionWrongStartIndex() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> Sort.partition(filmsList, -1, 3));
    }

    @Test
    void partitionStartIndexGreaterThanEnd() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        assertThrows(IllegalArgumentException.class, () -> Sort.partition(filmsList, 1, 0));
    }

    @Test
    void bucketSort() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film6_1);
        filmsList.add(film3);
        filmsList.add(film9);
        filmsList.add(film6_2);
        filmsList.add(film1);
        Sort.bucketSort(filmsList);
        assertTrue(Sort.checkSort(filmsList));
    }

    @Test
    void checkSort() {
        Film film1 = new Film("Test1", 5);
        Film film2 = new Film("Test2", 5);
        Film film3 = new Film("Test3", 7);
        Film film4 = new Film("Test4", 3);
        List<Film> sorted = new ArrayList<>(4);
        sorted.add(film4);
        sorted.add(film1);
        sorted.add(film2);
        sorted.add(film3);
        assertTrue(Sort.checkSort(sorted));
        List<Film> notSorted = new ArrayList<>(4);
        notSorted.add(film4);
        notSorted.add(film3);
        notSorted.add(film2);
        notSorted.add(film1);
        assertFalse(Sort.checkSort(notSorted));
    }
}