package org.example;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    @Test
    void filterList() {
        String[] film1 = {"1", "Title 1", ""};
        String[] film2 = {"2", "Title 2", "5.0"};
        String[] film3 = {"3", "Title 3", "3.0"};
        String[] film4 = {"4", "Title 4", ""};
        String[] film5 = {"5", "Title 5", "9.0"};
        List<String[]> filmsList = new ArrayList<>();
        filmsList.add(film1);
        filmsList.add(film2);
        filmsList.add(film3);
        filmsList.add(film4);
        filmsList.add(film5);
        Main.filterList(filmsList);
        for (String[] line : filmsList)
            assertNotEquals("", line[2]);
    }

    @Test
    void convertListToFilms() {
        String[] film1 = {"1", "Title 1", "7.0"};
        String[] film2 = {"2", "Title 2", "5.0"};
        String[] film3 = {"3", "Title 3", "3.0"};
        String[] film4 = {"4", "Title 4", "6.0"};
        String[] film5 = {"5", "Title 5", "9.0"};
        List<String[]> stringsList = new ArrayList<>();
        stringsList.add(film1);
        stringsList.add(film2);
        stringsList.add(film3);
        stringsList.add(film4);
        stringsList.add(film5);
        List<Film> filmsList = Main.convertListToFilms(stringsList);
        for (int i = 0; i < filmsList.size(); i++) {
            assertEquals(filmsList.get(i).getRating(), Double.valueOf(stringsList.get(i)[2]).intValue());
            assertEquals(filmsList.get(i).getTitle(), stringsList.get(i)[1]);
        }
    }

    @Test
    void meanRank() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film1);
        filmsList.add(film3);
        filmsList.add(film6_1);
        filmsList.add(film6_2);
        filmsList.add(film9);
        assertEquals(5, Main.meanRank(filmsList));
        filmsList.remove(2);
        assertEquals(4.75, Main.meanRank(filmsList));
    }

    @Test
    void medianRank() {
        Film film1 = new Film("Test1", 1);
        Film film3 = new Film("Test3", 3);
        Film film6_1 = new Film("Test6_1", 6);
        Film film6_2 = new Film("Test6_2", 6);
        Film film9 = new Film("Test9", 9);
        List<Film> filmsList = new ArrayList<>(5);
        filmsList.add(film1);
        filmsList.add(film3);
        filmsList.add(film6_1);
        filmsList.add(film6_2);
        filmsList.add(film9);
        assertEquals(6, Main.medianRank(filmsList));
        filmsList.remove(2);
        assertEquals(4.5, Main.medianRank(filmsList));
    }

    @Test
    void parseNonExistingCSV() {
        Main.parseCSV("test.csv");
    }

    @Test
    void parseCSV() {
        String[] strings = {"1", "Test 1", "5.0", "2", "Test 2", "7.0", "3", "Test 3", "8.0"};
        try {
            File testFile = new File("src/test/java/org/example/test.csv");
            FileWriter writeFile = new FileWriter(testFile.getPath());
            for (int i = 0; i < strings.length; i+=3)
                writeFile.write(strings[i] + "," + strings[i+1] + "," + strings[i+2] + "\n");
            writeFile.close();
            List<String[]> stringsList = Main.parseCSV(testFile.getPath());
            int i = 3;
            for (String[] line : stringsList) {
                assertEquals(line[0], strings[i]);
                assertEquals(line[1], strings[i+1]);
                assertEquals(line[2], strings[i+2]);
                i += 3;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}