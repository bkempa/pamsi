#include "sort.hh"
#include <iostream>
#include <chrono>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <boost/tokenizer.hpp>

#define DATA_SIZE 1010292


float mean_rank(film arr[], int size){
    int sum = 0;
    float mean;
    for (int i = 0; i < size; ++i)
        sum += arr[i].get_rating();
    mean = (float)sum / (float)size;
    return mean;
}

float median_sorted_arr(film arr[], const int &size){
    if (size % 2 == 0)
        return (arr[size/2].get_rating() + arr[size/2 - 1].get_rating())/2.0; 
    else
        return arr[size/2].get_rating();
}


void quick(film arr[], const int &size){
    film* arr_to_sort = new film[size];
    std::copy(&arr[0], &arr[size], &arr_to_sort[0]);
    auto start = std::chrono::high_resolution_clock::now();
    sort<film>::quick_sort(arr_to_sort, 0, size-1);
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_s = finish - start;
    if(sort<film>::check_sort(arr_to_sort, size))
        std::cout << "Tablica " << size << " elementow zostala posortowana prawidlowo.\n";
    else
        std::cout << "Tablica " << size << " elementow nie zostala posortowana.\n";
    std::cout << "Czas potrzebny na posortowanie tablicy " << size << " elementow: " << time_s.count() << " s.\n";
    float median = median_sorted_arr(arr_to_sort, size);
    std::cout << "Mediana rankingu: " << median << std::endl;
    float mean = mean_rank(arr_to_sort, size);
    std::cout << "Srednia wartosc rankingu: " << std::setprecision(3) << mean << std::endl << std::endl;
    delete[] arr_to_sort;
}

void merge(film arr[], const int &size){
    film* arr_to_sort = new film[size];
    std::copy(&arr[0], &arr[size], &arr_to_sort[0]);
    auto start = std::chrono::high_resolution_clock::now();
    sort<film>::merge_sort(arr_to_sort, 0, size-1);
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_s = finish - start;
    if(sort<film>::check_sort(arr_to_sort, size))
        std::cout << "Tablica " << size << " elementow zostala posortowana prawidlowo.\n";
    else
        std::cout << "Tablica " << size << " elementow nie zostala posortowana.\n";
    std::cout << "Czas potrzebny na posortowanie tablicy " << size << " elementow: " << time_s.count() << " s.\n";
    float median = median_sorted_arr(arr_to_sort, size);
    std::cout << "Mediana rankingu: " << median << std::endl;
    float mean = mean_rank(arr_to_sort, size);
    std::cout << "Srednia wartosc rankingu: " << std::setprecision(3) << mean << std::endl << std::endl;
    delete[] arr_to_sort;
}

void bucket(film arr[], const int &size){
    film* arr_to_sort = new film[size];
    std::copy(&arr[0], &arr[size], &arr_to_sort[0]);
    auto start = std::chrono::high_resolution_clock::now();
    sort<film>::bucket_sort(arr_to_sort, size);
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_s = finish - start;
    if(sort<film>::check_sort(arr_to_sort, size))
        std::cout << "Tablica " << size << " elementow zostala posortowana prawidlowo.\n";
    else
        std::cout << "Tablica " << size << " elementow nie zostala posortowana.\n";
    std::cout << "Czas potrzebny na posortowanie tablicy " << size << " elementow: " << time_s.count() << " s.\n";
    float median = median_sorted_arr(arr_to_sort, size);
    std::cout << "Mediana rankingu: " << median << std::endl;
    float mean = mean_rank(arr_to_sort, size);
    std::cout << "Srednia wartosc rankingu: " << std::setprecision(3) << mean << std::endl << std::endl;
    delete[] arr_to_sort;
}


std::vector<film> filter(film arr[], const int& size){
    std::vector<film> filtered;
    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < size; ++i){
        if (arr[i].get_rating() != 0)
            filtered.push_back(arr[i]);

        if (i == 999){
            auto finish = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double, std::milli> time = finish - start;
            std::cout << "Czas potrzebny na przefiltrowanie 1000 danych: " << time.count() << " ms.\n";
        }
        if (i == 9999){
            auto finish = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double, std::milli> time = finish - start;
            std::cout << "Czas potrzebny na przefiltrowanie 10 000 danych: " << time.count() << " ms.\n";
        }
        if (i == 99999){
            auto finish = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double, std::milli> time = finish - start;
            std::cout << "Czas potrzebny na przefiltrowanie 100 000 danych: " << time.count() << " ms.\n";
        }
        if (i == 499999){
            auto finish = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double, std::milli> time = finish - start;
            std::cout << "Czas potrzebny na przefiltrowanie 500 000 danych: " << time.count() << " ms.\n";
        }
        if (i == 999999){
            auto finish = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double, std::milli> time = finish - start;
            std::cout << "Czas potrzebny na przefiltrowanie 1 000 000 danych: " << time.count() << " ms.\n";
        }
    }    
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> time = finish - start;
    std::cout << "Liczba danych po przefiltrowaniu: " << filtered.size() << std::endl;
    std::cout << "Czas potrzebny na przefiltrowanie wszystkich (" << DATA_SIZE << ") danych: " << time.count() << " ms.\n\n";
    delete[] arr;
    return filtered;
}

film* parse_csv(std::ifstream &input_file){
    film* parsed = new film[DATA_SIZE];
    typedef boost::tokenizer<boost::escaped_list_separator<char>> Tokenizer;
    std::vector<std::string> vec;
    std::string line;
    int i = 0;
    getline(input_file,line);
    while (getline(input_file,line)){
        Tokenizer tok(line);
        vec.assign(tok.begin(),tok.end());
        parsed[i] = film(vec[1], vec[2]);
        ++i;
    }
    return parsed;
}


int main(){
    std::string data("projekt2_dane.csv");
    std::ifstream in(data.c_str());
    if (!in.is_open())
        return 1;
    std::cout << "Trwa wczytywanie bazy danych...\n";
    film* movie_base = parse_csv(in);
    std::cout << "Baza zostala wczytana\n";    
    std::cout << "\t\t\tFiltrowanie\n";
    std::vector<film> filtered_movies = filter(movie_base, DATA_SIZE);
    
    film* arr_10_000_orig = new film[10000];    
    film* arr_100_000_orig = new film[100000];
    film* arr_500_000_orig = new film[500000];
    film* arr_1_000_000_orig = new film[1000000];
    film* arr_max_orig = new film[DATA_SIZE];


    for (long unsigned int i = 0; i < filtered_movies.size(); ++i){
        if (i < 10000)
            arr_10_000_orig[i] = filtered_movies[i];
        if (i < 100000)
            arr_100_000_orig[i] = filtered_movies[i];
        if (i < 500000)
            arr_500_000_orig[i] = filtered_movies[i];
        arr_1_000_000_orig[i] = filtered_movies[i];
        arr_max_orig[i] = filtered_movies[i];
    }
    int j = 0;
    for (long unsigned int i = filtered_movies.size(); i < DATA_SIZE; ++i){
        if (i < 1000000)
            arr_1_000_000_orig[i] = filtered_movies[j];
        arr_max_orig[i] = filtered_movies[j];
        ++j;
    }
    film* arr_sort[5] = {arr_10_000_orig, arr_100_000_orig, arr_500_000_orig, arr_1_000_000_orig, arr_max_orig};
    int sizes[5] = {10000, 100000, 500000, 1000000, DATA_SIZE};
    std::cout << "\t\t\tQuicksort\n";
    for (int i = 0; i < 5; ++i)
        quick(arr_sort[i], sizes[i]);
    std::cout << "\n\t\t\tMerge sort\n";
    for (int i = 0; i < 5; ++i)
        merge(arr_sort[i], sizes[i]);
    std::cout << "\n\t\t\tBucket sort\n";
    for (int i = 0; i < 5; ++i)
        bucket(arr_sort[i], sizes[i]);
}