#ifndef SORT_HH
#define SORT_HH

#include "film.hh"
#include <vector>

template <class TYPE>
class sort {
    sort() {};
    public:
        static void merge_sort(TYPE arr[], const int &start, const int &end);
        static void merge(TYPE arr[], const int &start, const int &middle, const int &end);

        static void quick_sort(TYPE arr[], const int &start, const int &end);
        static int partition(TYPE arr[], const int &id1, const int &id2);    
        static void swap(TYPE arr[], const int &id1, const int &id2);
        static int random_pivot(TYPE arr[], const int &start, const int &end);

        static void bucket_sort(TYPE arr[], const int &size);

        static bool check_sort(TYPE arr[], const int &size);
};


/********************************************************************************
*                                                                               *
*                               Bucket sort                                     *
*                                                                               *
********************************************************************************/

//Bucket sort specialized for films sorting (34th line)
template <>
void sort<film>::bucket_sort(film arr[], const int &size){
    std::vector<film> bucket[10];
    for (int i = 0; i < size; ++i)
        bucket[arr[i].get_rating()-1].push_back(arr[i]);
    for (int i = 0; i < size; ++i)
        for (int j = 0; j < 10; ++j)
            for (long unsigned int k = 0; k < bucket[j].size(); ++k){
                arr[i] = bucket[j][k];
                ++i;
            }
}

template <class TYPE>
void sort<TYPE>::bucket_sort(TYPE arr[], const int &size){
    std::vector<TYPE> bucket[10];
    for (int i = 0; i < size; ++i)
        bucket[arr[i]-1].push_back(arr[i]);
    for (int i = 0; i < size; ++i)
        for (int j = 0; j < 10; ++j)
            for (long unsigned int k = 0; k < bucket[j].size(); ++k){
                arr[i] = bucket[j][k];
                ++i;
            }
}


/********************************************************************************
*                                                                               *
*                               Mergesort                                       *
*                                                                               *
********************************************************************************/

template <class TYPE>
void sort<TYPE>::merge(TYPE arr[], const int &start, const int &middle, const int &end) {
    int i, j, k, left_size, right_size;
    left_size = middle - start + 1;
    right_size = end - middle;
    TYPE* left_arr = new TYPE[left_size];
    TYPE* right_arr = new TYPE[right_size];
    for(i = 0; i < left_size; ++i)
        left_arr[i] = arr[start + i];
    
    for(j = 0; j < right_size; ++j)
        right_arr[j] = arr[middle + j + 1];
    
    i = j = 0;
    k = start;
    //merging temp arrays
    while(i < left_size && j < right_size) {
        if(!(left_arr[i] > right_arr[j])) {
            arr[k] = left_arr[i];
            i++;
        }
        else {
            arr[k] = right_arr[j];
            j++;
        }
        k++;
    }

    //remaining elements in temp arrays
    while(i < left_size) {
        arr[k] = left_arr[i];
        i++;
        k++;
    }
    while(j < right_size) {
        arr[k] = right_arr[j];
        j++;
        k++;
    }
    delete[] left_arr;
    delete[] right_arr;
}

template <class TYPE>
void sort<TYPE>::merge_sort(TYPE arr[], const int &start, const int &end) {
    if(start < end) {
        int middle = start + (end - start) / 2;
        merge_sort(arr, start, middle);
        merge_sort(arr, middle + 1, end);
        merge(arr, start, middle, end);
    }
}




/********************************************************************************
*                                                                               *
*                               Quicksort                                       *
*                                                                               *
********************************************************************************/

template <class TYPE>
int sort<TYPE>::partition(TYPE arr[], const int &id1, const int &id2){
    TYPE pivot = arr[id1];
    int i = id1 - 1;
    int j = id2 + 1;
 
    while (true) {
        do {
            i++;
        } while (arr[i] < pivot);
 
        do {
            j--;
        } while (arr[j] > pivot);
 
        if (i >= j)
            return j;
 
        swap(arr, i, j);
    }
}
 
template <class TYPE>
int sort<TYPE>::random_pivot(TYPE arr[], const int &start, const int &end){
    srand(time(NULL));
    int random = start + rand() % (end - start); 
    swap(arr, random, end);
    return partition(arr, start, end);
}
 
template <class TYPE>
void sort<TYPE>::quick_sort(TYPE arr[], const int &start, const int &end){
    if (start < end) {
        int ind = random_pivot(arr, start, end);
        quick_sort(arr, start, ind);
        quick_sort(arr, ind + 1, end);
    }
}

template <class TYPE>
void sort<TYPE>::swap(TYPE arr[], const int &id1, const int &id2){
    TYPE tmp = arr[id1];
    arr[id1] = arr[id2];
    arr[id2] = tmp;
}



template <class TYPE>
bool sort<TYPE>::check_sort(TYPE arr[], const int &size){
    for(int i = 1; i < size; ++i){
        if(arr[i-1] > arr[i])
            return false;
    }
    return true;
}

#endif