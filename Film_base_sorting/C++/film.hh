#ifndef FILM_HH
#define FILM_HH

#include <iostream>


class film {
    private:
        std::string title;
        int rating;
    public:
        film(){}
        film(std::string name, std::string rate);
        std::string get_title() const {return title;}
        int get_rating() const {return rating;}
        bool operator> (const film& f2){return (rating > f2.rating);}
        bool operator< (const film& f2){return (rating < f2.rating);}
        film(const film &p1) {title = p1.title; rating = p1.rating;}
};

film::film(std::string name, std::string rate){
    title=name;
    if(rate=="")
        rating = 0;
    else
        rating=std::stoi(rate);
}

std::ostream& operator<< (std::ostream &strm, const film &data) {
    strm << data.get_title() << " - " << data.get_rating();
    return strm;
}


#endif