
# Film base sorting

The program’s purpose is to focus on various sorting algorithms. Initially a CSV-
format file with over a million films with their ratings from the IMDB website is
parsed. Then, the base is filtered in order to remove films without the rating, preparing sets of 10 000, 100 000, 500 000 and 1 000 000 films on which implementations
efficiencies and time complexities of own algorithms (quicksort, mergesort and bucketsort) are tested.


## Features

- 3 sorting algorithms
- Sorting time measuring
- CSV parsing
- Cross platform


## Installation

### C++
To run this program you only need to type these commands in your Linux system terminal:
```bash
  chmod +x Makefile
  make
```

### Java
It is possible to run the program in multiple ways, e.g. by importing it and running it in your IDE or using [Exec Maven plugin](http://www.mojohaus.org/exec-maven-plugin/). To do it you should type in the command line:
```bash
  mvn exec:java -Dexec.mainClass="com.example.Main"
```

### Python
You are able to run the program via your IDE or command line (you need to have [Python](https://www.python.org/downloads/) installed first) using this command:
```bash
  python main.py
```
    
## Screenshots

[![filmbase1.png](https://i.postimg.cc/fLt4ctWV/filmbase1.png)](https://postimg.cc/R6Bb90Hm)

[![filmbase2.png](https://i.postimg.cc/4NTCcxD3/filmbase2.png)](https://postimg.cc/5jpkdbNc)

[![filmbase3.png](https://i.postimg.cc/NGXqLHPV/filmbase3.png)](https://postimg.cc/z3qPcVNS)

