
# Tic-tac-toe AI

Tic-tac-toe game (versions in Java and Python feature a graphical user interface).
Its special attribute is the possibility to choose the board size from a 3-10 range
and to select a number of symbols in a row required to win (from 3 to the board
size). Moreover, a minmax artificial intelligence algorithm has been implemented,
thus not only is a 2-player mode available, but also it is possible to play against
a computer.


## Features

- Player vs Player and Player vs AI modes
- Minmax AI algorithm
- Changing the board size
- Selecting number of symbols to win
- Cross platform


## Installation

### C++
To run this program you only need to type these commands in your Linux system terminal:
```bash
  chmod +x Makefile
  make
```

### Java
It is possible to run the program in multiple ways, e.g. by importing it and running it in your IDE or using [Exec Maven plugin](http://www.mojohaus.org/exec-maven-plugin/). To do it you should type in the command line:
```bash
  mvn exec:java -Dexec.mainClass="com.example.Main"
```

### Python
You are able to run the program via your IDE or command line (you need to have [Python](https://www.python.org/downloads/) installed first) using this command:
```bash
  python main.py
```
    
## Screenshots

### Choosing game mode
[![tic-tac-toe1.png](https://i.postimg.cc/mgb0d62f/tic-tac-toe1.png)](https://postimg.cc/1fjCXBqJ)
### Board
[![tic-tac-toe2.png](https://i.postimg.cc/13Jbbcfg/tic-tac-toe2.png)](https://postimg.cc/0zKtwSGx)
### Game over

[![tic-tac-toe4.png](https://i.postimg.cc/Znp1QGdZ/tic-tac-toe4.png)](https://postimg.cc/2LjcbXGX)

