from board import Board


class Game(Board):
    draw = 0
    win = 1000
    loss = -1000
    start_depth = 0
    score = 0

    def __init__(self, board_size, player1, player2, symbols_to_win, ai_game):
        super().__init__(board_size)
        self.ai_game = ai_game
        self.player1 = player1
        self.player2 = player2
        self.symbols_to_win = symbols_to_win
        if player1.first:
            self.current_turn_player = player1
        else:
            self.current_turn_player = player2

    def if_game_is_won(self, player_symbol):
        counter = 0
        # horizontal #
        for row in self.grid:
            for field in row:
                if field == player_symbol:
                    counter += 1
                else:
                    counter = 0

                if counter == self.symbols_to_win:
                    return True

            counter = 0

        # vertical #
        for field in range(self.board_size):
            for column in range(self.board_size):
                if self.grid[column][field] == player_symbol:
                    counter += 1
                else:
                    counter = 0

                if counter == self.symbols_to_win:
                    return True

            counter = 0

        # 1st diagonal (0, 0); (1, 1) etc. #
        for i in range(self.board_size):
            if self.grid[i][i] == player_symbol:
                counter += 1
            else:
                counter = 0

            if counter == self.symbols_to_win:
                return True

        counter = 0
        # 2nd diagonal (0, 2); (1, 1) etc. #
        for i in range(self.board_size):
            if self.grid[i][self.board_size - 1 - i] == player_symbol:
                counter += 1
            else:
                counter = 0

            if counter == self.symbols_to_win:
                return True

        # smaller diagonals #
        tmp = 1
        counter = 0
        for m in range(self.board_size - self.symbols_to_win):
            i = 0
            for j in range(tmp, self.board_size - self.symbols_to_win + 1):
                for k in range(self.symbols_to_win):
                    if self.grid[i][j] == player_symbol:
                        counter += 1
                    else:
                        counter = 0

                    if counter == self.symbols_to_win:
                        return True

                    j += 1
                    i += 1

                i -= (self.symbols_to_win - 1)
                j -= (self.symbols_to_win - 1)
                counter = 0

            tmp += 1

        tmp = 1
        for m in range(self.board_size - self.symbols_to_win):
            i = 0
            for j in range(tmp, self.board_size - self.symbols_to_win + 1):
                for k in range(self.symbols_to_win):
                    if self.grid[j][i] == player_symbol:
                        counter += 1
                    else:
                        counter = 0

                    if counter == self.symbols_to_win:
                        return True

                    j += 1
                    i += 1

                j -= (self.symbols_to_win - 1)
                i -= (self.symbols_to_win - 1)
                counter = 0

            tmp += 1

        tmp = self.board_size - 2
        for m in range(self.board_size - self.symbols_to_win):
            j = 0
            i = tmp
            while i > self.symbols_to_win - 2:
                for k in range(self.symbols_to_win):
                    if self.grid[i][j] == player_symbol:
                        counter += 1
                    else:
                        counter = 0

                    if counter == self.symbols_to_win:
                        return True

                    j += 1
                    i -= 1

                j -= (self.symbols_to_win - 1)
                i += (self.symbols_to_win - 1)
                counter = 0

            tmp -= 1

        tmp = 1
        for m in range(self.board_size - self.symbols_to_win):
            i = self.board_size - 1
            for j in range(tmp, self.board_size - self.symbols_to_win + 1):
                for k in range(self.symbols_to_win):
                    if self.grid[i][j] == player_symbol:
                        counter += 1
                    else:
                        counter = 0

                    if counter == self.symbols_to_win:
                        return True

                    j += 1
                    i -= 1

                i += self.symbols_to_win - 1
                j -= (self.symbols_to_win - 1)
                counter = 0

            tmp += 1

        return False

    def if_game_is_over(self):
        return self.is_board_full() or self.get_game_state(self.player1.player_symbol) != self.draw

    def get_game_state(self, player_symbol):
        opponent_symbol = self.get_opponent_symbol(player_symbol)
        if self.if_game_is_won(player_symbol):
            return self.win
        elif self.if_game_is_won(opponent_symbol):
            return self.loss
        else:
            return self.draw

    def get_opponent_symbol(self, player_symbol):
        if player_symbol == self.player1.player_symbol:
            return self.player2.player_symbol
        else:
            return self.player1.player_symbol

    def clear_board(self):
        for id1, row in enumerate(self.grid):
            for id2, field in enumerate(row):
                self.grid[id1][id2] = '-'

    def ai_move(self):
        ai_move = self.minmax(self.player2.player_symbol, self.start_depth, self.loss, self.win)
        if ai_move[1][0] != -1 and ai_move[1][1] != -1:
            self.grid[ai_move[1][0]][ai_move[1][1]] = self.player2.player_symbol
        return ai_move[1]

    def minmax(self, symbol, depth, alpha, beta):
        best_move = (-1, -1)
        best_score = self.loss if symbol == self.player2.player_symbol else self.win

        if self.is_board_full() or self.get_game_state(self.player2.player_symbol) != self.draw:
            best_score = self.get_game_state(self.player2.player_symbol)
            return best_score, best_move

        legal_moves = self.get_possible_moves()

        for current_move in legal_moves:
            self.grid[current_move[0]][current_move[1]] = symbol
            # Adjusting the tree's depth to reduce waiting for AI's turn time according to the chosen board size
            depth_limit = 10 if self.board_size == 3 else 5

            # Maximizing "max" player's turn
            if symbol == self.player2.player_symbol:
                if depth < depth_limit:
                    score = self.minmax(self.player1.player_symbol, depth + 1, alpha, beta)[0]
                    if best_score < score:
                        best_score = score - depth * 10
                        best_move = current_move
                        # Alpha-beta pruning
                        alpha = max(alpha, best_score)
                        self.grid[current_move[0]][current_move[1]] = '-'
                        if beta <= alpha:
                            break
            # Minimizing "min" player's turn
            else:
                if depth < depth_limit:
                    score = self.minmax(self.player2.player_symbol, depth + 1, alpha, beta)[0]
                    if best_score > score:
                        best_score = score + depth * 10
                        best_move = current_move
                        # Alpha-beta pruning
                        beta = min(beta, best_score)
                        self.grid[current_move[0]][current_move[1]] = '-'
                        if beta <= alpha:
                            break
            self.grid[current_move[0]][current_move[1]] = '-'  # undo the move

        return best_score, best_move

    def change_turn_player(self):
        if self.current_turn_player == self.player1:
            self.current_turn_player = self.player2
        else:
            self.current_turn_player = self.player1
