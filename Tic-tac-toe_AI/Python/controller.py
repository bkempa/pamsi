import game_over_gui
import start_game_gui
import board_gui
import game
import player


def exit_game():
    quit()


class Controller:
    def __init__(self):
        self.game_over_gui = None
        self.game = None
        self.board_gui = None
        self.start_game_GUI = start_game_gui.StartGameGUI()
        self.start_game_GUI.confirm_button.config(command=self.play_game)
        self.start_game_GUI.player_symbol_combobox.current(0)
        self.start_game_GUI.window.mainloop()

    def create_game(self):
        player1 = player.Player(player_symbol=self.start_game_GUI.player_symbol_combobox.get(),
                                first=self.start_game_GUI.player1_first.get(),
                                ai=False)
        if self.start_game_GUI.player_symbol_combobox.get() == 'X':
            player2 = player.Player(player_symbol='O',
                                    first=not self.start_game_GUI.player1_first.get(),
                                    ai=bool(self.start_game_GUI.ai_game_choice.get()))
        else:
            player2 = player.Player(player_symbol='X',
                                    first=not self.start_game_GUI.player1_first.get(),
                                    ai=bool(self.start_game_GUI.ai_game_choice.get()))
        self.game = game.Game(board_size=int(self.start_game_GUI.board_size_combobox.get()),
                              player1=player1,
                              player2=player2,
                              symbols_to_win=int(self.start_game_GUI.symbols_to_win_combobox.get()),
                              ai_game=bool(self.start_game_GUI.ai_game_choice.get()))
        self.start_game_GUI.window.destroy()
        self.board_gui = board_gui.BoardGUI(self.game)

    def play_game(self):
        self.create_game()
        if not self.game.player1.first and self.game.ai_game:
            self.make_ai_move()
            self.change_turn()
            self.add_command_to_available_spaces()
        else:
            for i in range(len(self.board_gui.grid_buttons)):
                self.board_gui.grid_buttons[i].config(command=lambda ind=i: self.grid_button_action(ind))

    def grid_button_action(self, index):
        self.player_move(index)
        if not self.game.if_game_is_over():
            self.change_turn()

        if self.game.ai_game:
            self.make_ai_move()
            if not self.game.if_game_is_over():
                self.change_turn()
                self.add_command_to_available_spaces()

        if self.game.if_game_is_over():
            player1_state = self.game.get_game_state(self.game.player1.player_symbol)
            if player1_state == self.game.win:
                self.game.player1.add_point()
            elif player1_state == self.game.loss:
                self.game.player2.add_point()

            self.board_gui.update_scoreboard(self.game.player1.points, self.game.player2.points)
            for button in self.board_gui.grid_buttons:
                button.config(command='')

            self.game_over_gui = game_over_gui.GameOverGUI(self.game.ai_game)
            self.game_over_gui.window.protocol("WM_DELETE_WINDOW", exit_game)
            self.game_over_gui.update_scoreboard(self.game.player1.points, self.game.player2.points)
            self.winner_display(player1_state)
            self.game_over_gui.play_again_btn.config(command=self.play_again)
            self.game_over_gui.change_mode_btn.config(command=self.change_game_mode)
            self.game_over_gui.exit_btn.config(command=exit_game)

    def player_move(self, index):
        if self.game.current_turn_player.player_symbol == 'X':
            self.board_gui.grid_buttons[index].config(fg='red', text='X', command='')
        else:
            self.board_gui.grid_buttons[index].config(fg='blue', text='O', command='')
        self.game.grid[index // self.game.board_size][index % self.game.board_size] = self.game.current_turn_player.player_symbol

    def change_turn(self):
        self.game.change_turn_player()
        self.board_gui.change_turn()

    def add_command_to_available_spaces(self):
        for button in self.board_gui.grid_buttons:
            button.config(command='')
        for i in range(self.game.board_size):
            for j in range(self.game.board_size):
                if self.game.grid[i][j] == '-':
                    self.board_gui.grid_buttons[self.game.board_size * i + j].config(
                        command=lambda ind=self.game.board_size * i + j: self.grid_button_action(ind))

    def make_ai_move(self):
        for button in self.board_gui.grid_buttons:
            button.config(command='')
        move = self.game.ai_move()
        if move[0] != -1 and move[1] != -1:
            self.game.grid[move[0]][move[1]] = self.game.player2.player_symbol
            if self.game.player2.player_symbol == 'X':
                self.board_gui.grid_buttons[move[0] * self.game.board_size + move[1]]. \
                    config(fg='red', text=self.game.player2.player_symbol, command='')
            else:
                self.board_gui.grid_buttons[move[0] * self.game.board_size + move[1]]. \
                    config(fg='blue', text=self.game.player2.player_symbol, command='')

    def winner_display(self, player1_state):
        if self.game.ai_game:
            if player1_state == self.game.loss:
                self.game_over_gui.winner_display.config(text="You lost")
            elif player1_state == self.game.win:
                self.game_over_gui.winner_display.config(text="You won!")
            else:
                self.game_over_gui.winner_display.config(text="Draw")
        else:
            if player1_state == self.game.loss:
                self.game_over_gui.winner_display.config(text="Player 2 won!")
            elif player1_state == self.game.win:
                self.game_over_gui.winner_display.config(text="Player 1 won!")
            else:
                self.game_over_gui.winner_display.config(text="Draw")

    def play_again(self):
        if self.game.player1.first:
            self.game.current_turn_player = self.game.player1
        else:
            self.game.current_turn_player = self.game.player2
        self.game.clear_board()
        self.board_gui.window.destroy()
        self.board_gui = board_gui.BoardGUI(self.game)
        for i in range(len(self.board_gui.grid_buttons)):
            self.board_gui.grid_buttons[i].config(command=lambda ind=i: self.grid_button_action(ind))
        self.board_gui.update_scoreboard(self.game.player1.points, self.game.player2.points)

    def change_game_mode(self):
        self.board_gui.window.destroy()
        self.__init__()
