from tkinter import ttk, Tk, PhotoImage, StringVar, Label, LEFT, TOP, Frame, Radiobutton, IntVar, LabelFrame, \
    Checkbutton, BooleanVar, Button


class StartGameGUI:
    def __init__(self):
        self.window = Tk()
        self.window.resizable(False, False)
        self.window.title("Tic-tac-toe")

        icon = PhotoImage(file='resources/tic-tac-toe-icon.png')
        self.window.iconphoto(True, icon)

        # board size #
        frame_brd_size = Frame(self.window, padx=50)
        frame_brd_size.pack(side=TOP)
        label_brd_size = Label(frame_brd_size, text="Choose board size: ", font=('Segue UI', 12), pady=10)
        label_brd_size.pack(side=LEFT)
        self.board_size_combobox = ttk.Combobox(frame_brd_size,
                                                font=('Segue UI', 12),
                                                width=2)
        self.board_size_combobox['values'] = [i for i in range(3, 11)]
        self.board_size_combobox['state'] = 'readonly'
        self.board_size_combobox.current(0)
        self.board_size_combobox.pack(side=LEFT)

        # symbols to win #
        frame_symbols_to_win = Frame(self.window, pady=10)
        frame_symbols_to_win.pack()
        label_symbols_to_win = Label(frame_symbols_to_win, text="Symbols in a row to win: ", font=('Segue UI', 10))
        label_symbols_to_win.pack(side=LEFT)
        self.symbols_to_win_combobox = ttk.Combobox(frame_symbols_to_win,
                                                    font=('Segue UI', 10),
                                                    width=2)
        self.symbols_to_win_combobox['values'] = 3
        self.symbols_to_win_combobox['state'] = 'readonly'
        self.symbols_to_win_combobox.current(0)
        self.symbols_to_win_combobox.pack(side=LEFT)

        # AI or PvP #
        self.ai_game_choice = IntVar()
        frame_ai_pvp = LabelFrame(self.window, text="Game mode", pady=5, padx=5)
        frame_ai_pvp.pack()
        game_modes = ["2 players", "AI"]
        pvp_radio = Radiobutton(frame_ai_pvp, text=game_modes[0], variable=self.ai_game_choice, value=False, padx=5)
        pvp_radio.pack(side=LEFT)
        ai_radio = Radiobutton(frame_ai_pvp, text=game_modes[1], variable=self.ai_game_choice, value=True, padx=5)
        ai_radio.pack(side=LEFT)

        # player 1's symbol #
        frame_player_symbol = Frame(self.window, pady=10)
        frame_player_symbol.pack()
        label_player_symbol = Label(frame_player_symbol, text="Choose Player 1's symbol: ", font=('Segue UI', 10))
        label_player_symbol.pack(side=LEFT)
        player_symbol_options = StringVar()
        self.player_symbol_combobox = ttk.Combobox(frame_player_symbol,
                                                   textvariable=player_symbol_options,
                                                   font=('Segue UI', 10),
                                                   width=2)
        self.player_symbol_combobox['values'] = ["X", "O"]
        self.player_symbol_combobox['state'] = 'readonly'
        self.player_symbol_combobox.pack(side=LEFT)

        # who moves first #
        self.player1_first = BooleanVar()
        frame_first_move = Frame(self.window, pady=10)
        frame_first_move.pack()
        label_first_move = Label(frame_first_move, text="Do you want to move first?", font=('Segue UI', 10))
        first_move_button = Checkbutton(frame_first_move,
                                        variable=self.player1_first,
                                        onvalue=True,
                                        offvalue=False,
                                        font=('Segue UI', 10))
        label_first_move.pack(side=LEFT)
        first_move_button.select()
        first_move_button.pack(side=LEFT)

        # confirm settings #
        frame_confirm = Frame(self.window)
        frame_confirm.pack(pady=15)
        self.confirm_button = Button(frame_confirm, text="Confirm")
        self.confirm_button.pack()

        self.board_size_combobox.bind('<<ComboboxSelected>>',
                                      lambda event: self.update_symbols_to_win(self.board_size_combobox.get()))

        pvp_radio.select()

    def update_symbols_to_win(self, current_size):
        self.symbols_to_win_combobox['values'] = [i for i in range(3, int(current_size) + 1)]
