class Board:
    def __init__(self, board_size):
        self.board_size = board_size
        if board_size in range(3, 11):
            self.grid = [[] for _ in range(board_size)]
            for row in self.grid:
                for column in range(board_size):
                    row.append('-')
        else:
            raise Exception("Incorrect board size (should be in range [3, 10])")

    def get_possible_moves(self):
        possible_moves = []
        for ind1, row in enumerate(self.grid):
            for ind2, field in enumerate(row):
                if field == '-':
                    possible_moves.append((ind1, ind2))
        return possible_moves

    def is_board_full(self):
        for row in self.grid:
            for field in row:
                if field == '-':
                    return False
        return True
