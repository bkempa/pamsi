class Player:
    def __init__(self, player_symbol, first, ai):
        self.player_symbol = player_symbol
        self.first = first
        self.ai = ai
        self.points = 0

    def add_point(self):
        self.points += 1
