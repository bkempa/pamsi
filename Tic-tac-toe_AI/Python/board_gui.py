from tkinter import Tk, PhotoImage, Frame, Label, LEFT, Button
from tkinter.font import Font


class BoardGUI:
    def __init__(self, game):
        self.window = Tk()
        self.window.resizable(False, False)
        self.window.title("Tic-tac-toe")

        icon = PhotoImage(file='resources/tic-tac-toe-icon.png')
        self.window.iconphoto(True, icon)

        self.ai_game = game.ai_game

        # players symbols #
        frame_symbols = Frame(self.window, pady=5)
        frame_symbols.pack()
        player_names_font = Font(family='Segue UI', size=15)
        player_symbols_font = Font(family='Segue UI', size=15, weight='bold')
        if game.ai_game:
            player1_name = Label(frame_symbols, text="Player:", font=player_names_font)
            player2_name = Label(frame_symbols, text=", AI:", font=player_names_font)
        else:
            player1_name = Label(frame_symbols, text="Player 1:", font=player_names_font)
            player2_name = Label(frame_symbols, text=", Player 2:", font=player_names_font)
        player1_name.pack(side=LEFT)
        if game.player1.player_symbol == 'X':
            player1_symbol = Label(frame_symbols, text="X", font=player_symbols_font, fg='red')
            player2_symbol = Label(frame_symbols, text="O", font=player_symbols_font, fg='blue')
        else:
            player1_symbol = Label(frame_symbols, text="O", font=player_symbols_font, fg='blue')
            player2_symbol = Label(frame_symbols, text="X", font=player_symbols_font, fg='red')
        player1_symbol.pack(side=LEFT)
        player2_name.pack(side=LEFT)
        player2_symbol.pack(side=LEFT)

        # current turn #
        frame_turn = Frame(self.window)
        frame_turn.pack()
        if game.player1.first:
            self.current_turn = "Player 1"
        elif self.ai_game:
            self.current_turn = "AI"
        else:
            self.current_turn = "Player 2"
        self.current_turn_display = Label(frame_turn, text=self.current_turn + "'s turn", font=('Segue UI', 9, 'bold'))
        self.current_turn_display.pack()

        # grid frame #
        frame_grid = Frame(self.window)
        frame_grid.pack()
        self.grid_buttons = [Button(frame_grid,
                                    font=('Segue UI', 67 - (game.board_size - 3) * 5, 'bold'),
                                    width=3,
                                    height=1)
                             for _ in range(game.board_size ** 2)]
        for i in range(len(self.grid_buttons)):
            self.grid_buttons[i].grid(row=i // game.board_size, column=i % game.board_size)

        # scores #
        frame_scores = Frame(self.window, pady=5)
        frame_scores.pack()
        player1_score = player2_score = 0
        if self.ai_game:
            self.scores = Label(frame_scores, text="Player: " + str(player1_score) + ", AI: " + str(player2_score),
                                font=('Segue UI', 15))
        else:
            self.scores = Label(frame_scores, text="Player 1: " + str(player1_score) + ", Player 2: " + str(player2_score),
                                font=('Segue UI', 15))
        self.scores.pack()

    def change_turn(self):
        if self.current_turn == "Player 2" or self.current_turn == "AI":
            self.set_current_turn("Player 1")
        elif self.current_turn == "Player 1" and self.ai_game:
            self.set_current_turn("AI")
        else:
            self.set_current_turn("Player 2")

    def set_current_turn(self, current_turn):
        self.current_turn = current_turn
        self.current_turn_display.config(text=current_turn + "'s turn")

    def update_scoreboard(self, player1_score, player2_score):
        if self.ai_game:
            self.scores.config(text="Player: " + str(player1_score) + ", AI: " + str(player2_score))
        else:
            self.scores.config(text="Player 1: " + str(player1_score) + ", Player 2: " + str(player2_score))
