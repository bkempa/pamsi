from tkinter import PhotoImage, Frame, Label, Button, LEFT, Toplevel
from tkinter.font import Font


class GameOverGUI:
    def __init__(self, ai_game):
        self.ai_game = ai_game
        self.window = Toplevel()
        self.window.resizable(False, False)
        self.window.title("Tic-tac-toe")

        icon = PhotoImage(file='resources/tic-tac-toe-icon.png')
        self.window.iconphoto(True, icon)

        # winner #
        frame_winner = Frame(self.window, pady=2)
        frame_winner.pack()
        self.winner_display = Label(frame_winner, font=('Segue UI', 12))
        self.winner_display.pack()

        # scores #
        frame_scores = Frame(self.window)
        frame_scores.pack()
        self.player1_name = Label(frame_scores, font=('Segue UI', 12))
        self.player1_name.pack(side=LEFT)
        self.player1_score = Label(frame_scores, font=('Segue UI', 12, 'bold'))
        self.player1_score.pack(side=LEFT)
        self.player2_name = Label(frame_scores, font=('Segue UI', 12))
        self.player2_name.pack(side=LEFT)
        self.player2_score = Label(frame_scores, font=('Segue UI', 12, 'bold'))
        self.player2_score.pack(side=LEFT)

        # menu buttons #
        frame_buttons = Frame(self.window, padx=15, pady=15)
        frame_buttons.pack()
        menu_font = Font(size=12, family='Segue UI')
        self.play_again_btn = Button(frame_buttons, text="Play again", font=menu_font, width=20)
        self.play_again_btn.pack()
        self.change_mode_btn = Button(frame_buttons, text="Change mode", font=menu_font, width=20)
        self.change_mode_btn.pack()
        self.exit_btn = Button(frame_buttons, text="Exit", font=menu_font, width=20)
        self.exit_btn.pack()

    def update_scoreboard(self, player1_score, player2_score):
        if self.ai_game:
            self.player1_name.config(text="Player:")
            self.player1_score.config(text=str(player1_score) + ",")
            self.player2_name.config(text="AI:")
            self.player2_score.config(text=str(player2_score))
        else:
            self.player1_name.config(text="Player 1:")
            self.player1_score.config(text=str(player1_score) + ",")
            self.player2_name.config(text="Player 2:")
            self.player2_score.config(text=str(player2_score))
