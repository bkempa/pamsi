#include "game.hh"
#include <algorithm>

#define WIN 1000
#define	DRAW 0
#define LOSS -1000
#define START_DEPTH 0


//print if the player lost, won or drew
void game::print_game_state(const int &state){
	if (player1.get_ai() == player2.get_ai()){
		if (WIN == state){
			player1.set_points();
			std::cout << "\t\tGracz 1 wygral" << std::endl << std::endl;
		}
		else if (DRAW == state)
			std::cout << "\t\tRemis" << std::endl << std::endl;
		else if (LOSS == state){
			player2.set_points();
			std::cout << "\t\tGracz 2 wygral" << std::endl << std::endl;
		}
	}
	else {
		if (WIN == state){
			player1.set_points();
			std::cout << "\t\tZwyciestwo" << std::endl << std::endl;
		}
		else if (DRAW == state)
			std::cout << "\t\tRemis" << std::endl << std::endl;
		else if (LOSS == state){
			player2.set_points();
			std::cout << "\t\tPorazka" << std::endl << std::endl;
		}
	}
}

// Check if the game has been won - compares current board arrangement with calculated winning configurations
bool game::game_is_won(const std::vector<std::pair<int, int>> &occupied_positions) const {
	bool game_won;
	for (unsigned int i = 0; i < win_options.size(); i++){
		game_won = true;
		std::vector<std::pair<int, int>> curr_win_state = win_options[i];
		for (int j = 0; j < symb2win; j++){
			if ((std::find(std::begin(occupied_positions), std::end(occupied_positions), curr_win_state[j]) == std::end(occupied_positions))){
				game_won = false;
				break;
			}
		}

		if (game_won)
			break;
	}
	return game_won;
}

//Check if the game is finished
bool game::game_is_over() const {
	if (this->board_is_full())
		return true;

	if (DRAW != this->get_game_state(player1.get_symbol()))
		return true;

	return false;
}

//Check if someone has won, lost or drew
int game::get_game_state(const char &symbol) const {

	const char opponent_symbol = this->get_opponent_symbol(symbol);

	std::vector<std::pair<int, int>> occupied_positions = this->get_occupied_positions(symbol);
	bool is_won = this->game_is_won(occupied_positions);

	if (is_won)
		return WIN;

	occupied_positions = this->get_occupied_positions(opponent_symbol);
	bool is_lost = this->game_is_won(occupied_positions);

	if (is_lost)
		return LOSS;

	bool is_full = this->board_is_full();
	if (is_full)
		return DRAW;

	return DRAW;

}

char game::get_opponent_symbol(const char &symbol) const {
	char opponent_symbol;
	if (symbol == player1.get_symbol())
		opponent_symbol = player2.get_symbol();
	else
		opponent_symbol = player1.get_symbol();
	return opponent_symbol;
}

bool game::player_move(const player &plr){
	unsigned int row, col;
	std::cout << "Wybierz wiersz: ";
	std::cin >> row;
	while(row < 1 || row > this->get_board_dimension()){
		std::cout << "Podaj poprawna wartosc (min. 1, max. " << this->get_board_dimension() << "): ";
		std::cin >> row;
	}
	std::cout << "Wybierz kolumne: ";
	std::cin >> col;
	while(col < 1 || col > this->get_board_dimension()){
		std::cout << "Podaj poprawna wartosc (min. 1, max. " << this->get_board_dimension() << "): ";
		std::cin >> col;
	}
	std::cout << std::endl << std::endl;

	if (this->position_occupied(std::make_pair(row-1, col-1))){
		std::cout << "Podana pozycja (" << row << ", " << col << ") jest juz zajeta. Sprobuj jeszcze raz." << std::endl << std::endl;
		return false;
	}
	else
		this->set_spaces(row-1, col-1, plr.get_symbol());
	return true;
}

void game::ai_move(){
	std::pair<int, std::pair<int, int>> ai_move = this->minimax(player2.get_symbol(), START_DEPTH, LOSS, WIN);
	if(ai_move.second.first != -1 && ai_move.second.second != -1)
		this->set_spaces(ai_move.second.first, ai_move.second.second, player2.get_symbol());
}

int game::play(){
	bool place_available;
	this->clear_board();
	if(player1.get_ai() == player2.get_ai())
    	std::cout << "\n\tGracz 1 = X\tGracz 2 = O" << std::endl << std::endl;
	else
		std::cout << "\n\tGracz = " << player1.get_symbol() << "\tKomputer = " << player2.get_symbol() << std::endl << std::endl;
	
    while (!this->game_is_over()){
		if (player1.get_ai() == player2.get_ai()){	//2-player mode
			this->print_board();
			std::cout << std::endl;
			if (!game_is_over()){
				if(player1.get_first() == true){
					std::cout << "\tRuch gracza nr 1" << std::endl;
					do {
						place_available = this->player_move(player1);
					} while (place_available == false);
				}
				else {
					std::cout << "\tRuch gracza nr 2" << std::endl;
					do {
						place_available = this->player_move(player2);
					} while (place_available == false);
				}
			}
			this->print_board();
			std::cout << std::endl;
			if (!game_is_over()){
				if(player1.get_first() == true){
					std::cout << "\tRuch gracza nr 2" << std::endl;
					do {
						place_available = this->player_move(player2);
					} while (place_available == false);
				}
				else {
					std::cout << "\tRuch gracza nr 1" << std::endl;
					do {
						place_available = this->player_move(player1);
					} while (place_available == false);
				}
			}
		}
		else {	//vs AI
			if(player1.get_first() == true){
				this->print_board();
				std::cout << std::endl;
				std::cout << "\tTwoja kolej" << std::endl;
				if (this->player_move(player1) == false)
					continue;
				std::cout << "\tRuch komputera..." << std::endl << std::endl;
				this->ai_move();
			}
			else {
				this->print_board();
				std::cout << std::endl;
				std::cout << "\tRuch komputera..." << std::endl << std::endl;
				this->ai_move();
				this->print_board();
				std::cout << std::endl;
				if (!game_is_over()){
					std::cout << "\tTwoja kolej" << std::endl;
					do {
						place_available = this->player_move(player1);
					} while (place_available == false);
				}
			}
		}
	}
	this->print_board();
	std::cout << "*************** KONIEC ***************" << std::endl << std::endl;

	int player_state = this->get_game_state(player1.get_symbol());
	this->print_game_state(player_state);
	std::cout << "\t\tWyniki\n\t";
	if(player1.get_ai() == player2.get_ai())
		std::cout << "Gracz 1: " << player1.get_points() << "\tGracz 2: " << player2.get_points() << std::endl << std::endl;
	else
		std::cout << "Gracz: " << player1.get_points() << "\tKomputer: " << player2.get_points() << std::endl << std::endl;
	std::cout << "\t1) Zagraj ponownie\n\t2) Zmien typ gry\n\t3) Koniec gry\n\nTwoj wybor: ";
	int choice = 0, game_status;
	while(choice != 1 && choice != 2 && choice != 3){
		std::cin >> choice;
		switch(choice){
			case 1: game_status = 0; break;
			case 2: game_status = 1; break;
			case 3: game_status = -1; break;
			default: std::cout << "Wybierz poprawna opcje: "; break;
		}
	}
	if(game_status == 0 && player1.get_ai() == player2.get_ai()){	//if 2-player mode, next game with inverted order
		player1.set_first(!player1.get_first());
		player2.set_first(!player2.get_first());
	}
	return game_status;
}

//Minimax game optimization algorithm with alpha-beta pruning
std::pair<int, std::pair<int, int>> game::minimax(const char &symbol, const int &depth, int alpha, int beta){
	std::pair<int, int> best_move = std::make_pair(-1, -1);
	int best_score = (symbol == player2.get_symbol()) ? LOSS : WIN;

	if (this->board_is_full() || DRAW != this->get_game_state(player2.get_symbol())){
		best_score = this->get_game_state(player2.get_symbol());
		return std::make_pair(best_score, best_move);
	}

	std::vector<std::pair<int, int>> legal_moves = this->get_possible_moves();

	for(unsigned int i = 0; i < legal_moves.size(); i++){
		std::pair<int, int> curr_move = legal_moves[i];
		this->set_spaces(curr_move.first, curr_move.second, symbol);
		static int score;
		int depth_limit = 10;
		//Adjusting the tree's depth to reduce waiting for AI's turn time according to the chosen board size
		if(this->get_board_dimension() > 3)
			depth_limit = 5;
		//Maximizing "max" player's turn
		if(symbol == player2.get_symbol()){
			if(depth < depth_limit)
				score = this->minimax(player1.get_symbol(), depth + 1, alpha, beta).first;

			if (best_score < score){
				best_score = score - depth * 10;
				best_move = curr_move;
				//Alpha-beta pruning
				alpha = std::max(alpha, best_score);
				this->set_spaces(curr_move.first, curr_move.second, EMPTY_SPACE);
				if (beta <= alpha) 
					break; 
			}
		} //Minimizing "min" player's turn
		else {
			if(depth < depth_limit)
				score = this->minimax(player2.get_symbol(), depth + 1, alpha, beta).first;

			if (best_score > score){
				best_score = score + depth * 10;
				best_move = curr_move;
				//Alpha-beta pruning
				beta = std::min(beta, best_score);
				this->set_spaces(curr_move.first, curr_move.second, EMPTY_SPACE);
				if (beta <= alpha) 
					break; 
			}
		}
		this->set_spaces(curr_move.first, curr_move.second, EMPTY_SPACE); //Undo the move
	}
	return std::make_pair(best_score, best_move);
}