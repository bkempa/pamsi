#include <iostream>
#include <array>
#include "game.hh"

bool against_human_or_ai(){
	std::cout << "Wybierz tryb gry:\n\t1) Zagraj z komputerem\n\t2) Tryb dla 2 graczy\nTwoj wybor: ";
	int choice = 0;
	bool against_ai;
	while (choice != 1 && choice != 2) {
		std::cin >> choice;
		switch(choice){		
			case 1:
				against_ai = true; break;
			case 2:
				against_ai = false; break;
			default: std::cout << "Blad. Wybierz jedna z podanych opcji: "; break;
		}
	}
	return against_ai;
}

bool if_player_first(){
	char choice;
	bool plr_first = true;
	std::cout << "Czy chcesz wykonywac ruch jako pierwszy? (t/n): ";
	while(choice != 't' && choice != 'n'){
		std::cin >> choice;
		switch(choice){
			case 't': break;
			case 'n': plr_first = false; break;
			default: std::cout << "Sprobuj jeszcze raz (t/n): "; break;
		}
	}
	return plr_first;
}

std::array<char,2> choose_symbol(){
	std::array<char,2> symbols = {'a', 'b'};
	char choice;
	std::cout << "Wybierz, ktorym symbolem chcesz sie poruszac:\n\t1) X\n\t2) O\nTwoj wybor: ";
	while (symbols[0] != 'X' && symbols[0] != 'O'){
		std::cin >> choice;
		switch(choice){
			case '1': case 'X' : case 'x': symbols[0] = 'X'; symbols[1] = 'O'; break;
			case '2': case 'O' : case 'o': symbols[0] = 'O'; symbols[1] = 'X'; break;
			default: std::cout << "Blad. Wybierz jedna z dostepnych opcji: "; break;
		}
	}
	return symbols;
}

std::array<int,2> choose_game_size(){
	std::array<int,2> game_sizes;
	int size, symbols;
	std::cout << "Wybierz liczbe znakow w rzedzie (min. 3, max. 10): ";
	std::cin >> size;
	while(size < 3 || size > 10){
		std::cout << "Podaj poprawna wartosc (min. 3, max. 10): ";
		std::cin >> size;
	}
	if(size != 3){
		std::cout << "Wybierz liczbe znakow, ktore nalezy ulozyc w ciagu zeby wygrac (min. 3, max. " << size << "): ";
		std::cin >> symbols;
		while(symbols < 3 || symbols > size)
			std::cout << "Podaj poprawna wartosc (min. 3, max. " << size << "): ";
		game_sizes = {size, symbols};
	}
	else
		game_sizes = {size, size};
	return game_sizes;
}


int main(){
	int game_status;
	bool ai_game, player_first = true;
	std::array<char,2> players_symbols = {'X', 'O'}; //[0] - player1, [1] - player2
	std::array<int,2> game_parameters; //[0] - board size, [1] - symbols to win
	
	std::cout << "*********************************************\n\n\t\tKolko i krzyzyk\n\n*********************************************" << std::endl << std::endl;    
	ai_game = against_human_or_ai();
	if(ai_game == true){
		player_first = if_player_first();
		players_symbols = choose_symbol();
	}
	game_parameters = choose_game_size();
	player p1(players_symbols[0], player_first, false);
	player p2(players_symbols[1], !player_first, ai_game);
	game g(game_parameters[0], p1, p2, game_parameters[1]);
	do {
		game_status = g.play();
	} while (game_status == 0);	//player wants to play again
	while (game_status == 1){	//player wants to change game mode
		ai_game = against_human_or_ai();
		if(ai_game == true){
			player_first = if_player_first();
			players_symbols = choose_symbol();
		}
		else
			player_first = true;	//X-player starts
		game_parameters = choose_game_size();
		player p1(players_symbols[0], player_first, false);
		player p2(players_symbols[1], !player_first, ai_game);
		game g(game_parameters[0], p1, p2, game_parameters[1]);
		do {
			game_status = g.play();
		} while (game_status == 0);
	}
	return 0;
}