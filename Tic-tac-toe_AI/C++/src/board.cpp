#include "board.hh"
#include <iostream>

void board::print_board() const {
	//upper indexes
    std::cout << "\t      1";
    for (unsigned int i = 2; i <= board_dimension; i++){
        std::cout << "   " << i;
        if (i == board_dimension){
            std::cout << std::endl;
            std::cout << "\t    _____";
        }
    }
	//upper edge
    for (unsigned int i = 2; i <= board_dimension; i++){
        std::cout << "____";
        if (i == board_dimension){
            std::cout << std::endl;
            std::cout << "\t   |     ";
        }
    }
	//left edge
    for (unsigned int i = 2; i <= board_dimension; i++){
        std::cout << "    ";
        if (i == board_dimension)
            std::cout << "|" << std::endl;
    }
	//left indexes, left and right edges and board content
    for (unsigned int i = 0; i < board_dimension; ++i){
		if(i == 9)
			std::cout << "\t" << i+1 << " |  ";
		else
        	std::cout << "\t" << i+1 << "  |  ";
            for (unsigned int j = 0; j < board_dimension; ++j){
                std::cout << spaces[i][j];
                if (j != board_dimension-1)
                    std::cout << " | ";
                else
                    std::cout << "  |" << std::endl;
            }
	        
            if (i < board_dimension-1){
                std::cout << "\t   | ";
                for (unsigned int i = 2; i <= board_dimension; i++){
                    std::cout << "----";
                    if (i == board_dimension)
                        std::cout << "---";
                }
                std::cout << " |" << std::endl;
            }
    }
	//lower edge
    std::cout << "\t   |_____";
    for (unsigned int i = 2; i <= board_dimension; i++)
        std::cout << "____";
    std::cout << "|" << std::endl << std::endl;
}

//constructor
board::board(const int &size){
    board_dimension = size;
    std::vector<char> tmp;
	for (int i = 0; i < size; ++i)
		tmp.push_back(EMPTY_SPACE);
	for (int i = 0; i < size; ++i)
		spaces.push_back(tmp);
}

void board::clear_board(){
	for (unsigned int i = 0; i < board_dimension; ++i)
		for (unsigned int j = 0; j < board_dimension; ++j)
			spaces[i][j] = EMPTY_SPACE;
}

//Get all available legal moves (spaces that are not occupied)
const std::vector<std::pair<int, int>> board::get_possible_moves() const {
	std::vector<std::pair<int, int>> possible_moves;
	for (unsigned int i = 0; i < board_dimension; i++)
		for (unsigned int j = 0; j < board_dimension; j++)
			if (spaces[i][j] != 'X' && spaces[i][j] != 'O')
				possible_moves.push_back(std::make_pair(i, j));
	return possible_moves;
}

//Check if a position is occupied
bool board::position_occupied(const std::pair<int, int> &pos) const {
    //czy ten this jest git
	std::vector<std::pair<int, int>> possible_moves = this->get_possible_moves();
	for (unsigned int i = 0; i < possible_moves.size(); i++)
		if (pos.first == possible_moves[i].first && pos.second == possible_moves[i].second)
			return false;
	return true;
}

//Get all board positions occupied by the given symbol
std::vector<std::pair<int, int>> board::get_occupied_positions(const char &symbol) const {
	std::vector<std::pair<int, int>> occupied_positions;

	for (unsigned int i = 0; i < board_dimension; i++)
		for (unsigned int j = 0; j < board_dimension; j++)
			if (symbol == spaces[i][j])
				occupied_positions.push_back(std::make_pair(i, j));

	return occupied_positions;
}

//Check if the board is full
bool board::board_is_full() const {
	std::vector<std::pair<int, int>> possible_moves = this->get_possible_moves();
	if (possible_moves.size() == 0)
		return true;
	else
		return false;
}