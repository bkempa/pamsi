#ifndef GAME_HH
#define GAME_HH

#include "player.hh"
#include "board.hh"
#include <iostream>
#include <vector>

class game : public board {
	player player1;
	player player2;
	int symb2win;
	std::vector<std::vector<std::pair<int, int>>> win_options;
	public:
		game(const int &dim, player p1, player p2, const int &symbols2win) : board(dim), player1(p1), player2(p2), symb2win(symbols2win) {
			//win options
			std::vector<std::pair<int, int>> temp;
			int iter1 = 0, iter2 = symbols2win;
			//horizontal
			for(int i = 0; i < dim; ++i){
				for(int k = 0; k < dim-symbols2win+1; ++k){
					for(int j = iter1; j < iter2; ++j)
						temp.push_back(std::make_pair(i, j));
					++iter1; ++iter2;
					win_options.push_back(temp);
					temp.clear();
				}
				iter1 = 0; iter2 = symbols2win;
			}
			//vertical
			for(int i = 0; i < dim; ++i){
				for(int k = 0; k < dim-symbols2win+1; ++k){
					for(int j = iter1; j < iter2; ++j)
						temp.push_back(std::make_pair(j, i));
					++iter1; ++iter2;
					win_options.push_back(temp);
					temp.clear();
				}
				iter1 = 0; iter2 = symbols2win;
			}
			//1st diagonal (0,0); (1,1); (2,2) etc.
			for(int k = 0; k < dim-symbols2win+1; ++k){
				for(int j = iter1; j < iter2; ++j)
					temp.push_back(std::make_pair(j, j));
				win_options.push_back(temp);
				temp.clear();
				++iter1; ++iter2;
			}
			//2nd diagonal (3,0); (2,1); (1,2); (0,3)
			iter1 = dim-1; iter2 = dim-symbols2win+1;
			for(int k = 0; k < dim-symbols2win+1; ++k){
				int j = k;
				for(int i = iter1; i > iter2-2; --i){
					temp.push_back(std::make_pair(i, j));
					++j;
				}
				win_options.push_back(temp);
				temp.clear();
				--iter1; --iter2;
			}
			//smaller diagonals
			int i = 0, j = 0;
			int tmp = 1;
			
			for(int m = 0; m < dim-symb2win; ++m){
				i = 0;
				for(int j = tmp; j < dim-symb2win+1; j -= (symb2win-1)){
					for(int k = 0; k < symb2win; ++k){
						temp.push_back(std::make_pair(i, j));
						++j;
						++i;
					}
					win_options.push_back(temp);
					temp.clear();
					i -= (symb2win-1);
				}
				++tmp;
			}
			tmp = 1;
			for(int m = 0; m < dim-symb2win; ++m){
				i = 0;
				for(int j = tmp; j < dim-symb2win+1; j -= (symb2win-1)){
					for(int k = 0; k < symb2win; ++k){
						temp.push_back(std::make_pair(j, i));
						++j;
						++i;
					}
					win_options.push_back(temp);
					temp.clear();
					i -= (symb2win-1);
				}
				++tmp;
			}
			
			tmp = dim - 2;
			for(int m = 0; m < dim-symb2win; ++m){
				j = 0;
				for(i = tmp; i > symb2win-2; i += (symb2win-1)){
					for(int k = 0; k < symb2win; ++k){
						temp.push_back(std::make_pair(i, j));
						++j;
						--i;
					}
					win_options.push_back(temp);
					temp.clear();
					j -= (symb2win-1);
				}
				--tmp;
			}
			
			tmp = 1;
			j = 1;
			for(int m = 0; m < dim-symb2win; ++m){
				i = dim-1;
				for(j = tmp; j < dim-symb2win+1; j -= (symb2win-1)){
					for(int k = 0; k < symb2win; ++k){
						temp.push_back(std::make_pair(i, j));
						++j;
						--i;
					}
					win_options.push_back(temp);
					temp.clear();
					i += (symb2win-1);
				}
				++tmp;
			}
		};
		void print_game_state(const int &state);
		bool game_is_won(const std::vector<std::pair<int, int>> &occupied_positions) const;
		bool game_is_over() const;
		int get_game_state(const char &symbol) const;
		char get_opponent_symbol(const char &symbol) const;
		int play();
		bool player_move(const player &plr);
		void ai_move();
		std::pair<int, std::pair<int, int>> minimax(const char &symbol, const int &depth, int alpha, int beta);
};


#endif