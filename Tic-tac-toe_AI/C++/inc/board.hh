#ifndef BOARD_HH
#define BOARD_HH

#include <vector>
#define EMPTY_SPACE '-'

class board {
	unsigned int board_dimension;
	std::vector<std::vector<char>> spaces;
	public:
        board(const int &size);
		void print_board() const;
		void clear_board();
		const std::vector<std::pair<int, int>> get_possible_moves() const;
		bool position_occupied(const std::pair<int, int> &pos) const;
		std::vector<std::pair<int, int>> get_occupied_positions(const char &symbol) const;
		bool board_is_full() const;
		unsigned int get_board_dimension() const {return board_dimension;}
		std::vector<std::vector<char>> get_spaces() const {return spaces;}
		void set_spaces(const int &x, const int &y, const char &symbol){spaces[x][y] = symbol;};
};


#endif