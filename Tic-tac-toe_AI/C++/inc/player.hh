#ifndef PLAYER_HH
#define PLAYER_HH

class player {
	char symbol;
	unsigned int points;
	bool first;
	bool ai;
	public:
		player(const char &symb, const bool &if_first, const bool &ai_game) : symbol(symb), points(0), first(if_first), ai(ai_game){}
		unsigned int get_points() const {return points;};
		void set_points(){points++;};
		char get_symbol() const {return symbol;};
		bool get_first() const {return first;};
		bool get_ai() const {return ai;};
		void set_first(const bool &if_first){first = if_first;};
};



#endif