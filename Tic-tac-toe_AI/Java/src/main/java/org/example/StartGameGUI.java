package org.example;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;

@Getter
@Setter
public class StartGameGUI {
    private final JButton confirmSize;
    private final JComboBox<String> sizesBox;
    private final JComboBox<String> symbolsBox;
    private final JFrame frame;
    private final JRadioButton chooseAI;
    private final JCheckBox player1starts;
    private final JComboBox<String> symbolsToWin;
    private DefaultComboBoxModel<String> symbolsToWinModel;

    StartGameGUI() {
        frame = new JFrame("Tic-Tac-Toe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(300, 300));
        ImageIcon icon = new ImageIcon("src/main/resources/tic-tac-toe-icon.png");
        frame.setIconImage(icon.getImage());
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.setResizable(false);


        // size
        JLabel textField1 = new JLabel();
        textField1.setFont(new Font("Arial", Font.PLAIN, 18));
        textField1.setText("Choose board size:");
        JPanel sizeChoose = new JPanel();
        sizeChoose.setLayout(new FlowLayout());
        sizeChoose.add(textField1);
        String[] boardSizes = {"3", "4", "5", "6", "7", "8", "9", "10"};
        sizesBox = new JComboBox<>(boardSizes);
        sizesBox.setFont(new Font("Arial", Font.PLAIN, 18));
        sizesBox.setFocusable(false);
        sizeChoose.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        sizeChoose.add(sizesBox);
        frame.add(sizeChoose);

        // symbols to win
        JPanel symbolsToWinPanel = new JPanel();
        JLabel textField2 = new JLabel("Symbols in a row to win:");
        symbolsToWinPanel.add(textField2);
        String[] symbolsToWinOptions = new String[1];
        symbolsToWinOptions[0] = "3";
        symbolsToWinModel = new DefaultComboBoxModel<>(symbolsToWinOptions);
        symbolsToWin = new JComboBox<>(symbolsToWinModel);
        symbolsToWin.setFocusable(false);
        symbolsToWinPanel.add(symbolsToWin);
        frame.add(symbolsToWinPanel);


        // AI or PvP
        JPanel AIorPvP = new JPanel();
        JRadioButton choosePvP = new JRadioButton("2 players");
        choosePvP.setFocusable(false);
        choosePvP.setSelected(true);
        chooseAI = new JRadioButton("AI");
        chooseAI.setFocusable(false);
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(choosePvP);
        buttonGroup.add(chooseAI);
        AIorPvP.add(choosePvP);
        AIorPvP.add(chooseAI);
        AIorPvP.setMaximumSize(new Dimension(200, 30));
        AIorPvP.setBorder(BorderFactory.createTitledBorder("Game mode"));
        frame.add(AIorPvP);

        // symbol
        JLabel textfield2 = new JLabel();
        textfield2.setText("Choose Player 1's symbol:");
        JPanel symbolChoose = new JPanel();
        symbolChoose.setLayout(new FlowLayout());
        symbolChoose.add(textfield2);
        String[] symbols = {"X", "O"};
        symbolsBox = new JComboBox<>(symbols);
        symbolsBox.setFocusable(false);
        symbolChoose.add(symbolsBox);
        symbolChoose.setEnabled(false);
        frame.add(symbolChoose);

        // who starts
        JPanel whoStartsPanel = new JPanel();
        JLabel startText = new JLabel("Do you want to move first?");
        player1starts = new JCheckBox();
        player1starts.setFocusable(false);
        player1starts.setSelected(true);
        whoStartsPanel.add(startText);
        whoStartsPanel.add(player1starts);
        frame.add(whoStartsPanel);

        // confirm
        JPanel confirmPanel = new JPanel();
        confirmSize = new JButton();
        confirmSize.setText("Confirm");
        confirmSize.setFocusable(false);
        confirmPanel.add(confirmSize);
        frame.add(confirmPanel);

        frame.pack();
        frame.setVisible(true);
    }

    public void adjustSymbolsToWinToBoardSize() {
        String[] addedOptions = new String[sizesBox.getSelectedIndex() + 1];
        for (int i = 3; i <= sizesBox.getSelectedIndex() + 3; i++)
            addedOptions[i - 3] = String.valueOf(i);
        symbolsToWinModel = new DefaultComboBoxModel<>(addedOptions);
    }
}

