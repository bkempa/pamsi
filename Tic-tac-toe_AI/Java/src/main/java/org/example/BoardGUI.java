package org.example;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

@Getter
@Setter
public class BoardGUI {
    private final JLabel score;
    private final JButton[] gridButtons;
    private final JFrame frame;
    private String currentTurn;
    private final boolean AIgame;
    private final JLabel currentTurnDisplay;

    BoardGUI(Game game) {
        frame = new JFrame("Tic-Tac-Toe");
        ImageIcon icon = new ImageIcon("src/main/resources/tic-tac-toe-icon.png");
        frame.setIconImage(icon.getImage());
        frame.getContentPane().setBackground(Color.LIGHT_GRAY);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(480 + (game.getBoardSize() - 3) * 80, 480 + (game.getBoardSize() - 3) * 80);
        frame.setResizable(false);

        // players symbols
        AIgame = game.isAIgame();
        JPanel symbolsPanel = new JPanel();
        JLabel playersSymbols = new JLabel();
        playersSymbols.setFont(new Font("Arial", Font.PLAIN, 20 + (game.getBoardSize() - 3) * 2));
        if (AIgame && game.getPlayer1().getPlayerSymbol().equals("X"))
            playersSymbols.setText("<html>Player: <font color=red><strong>X</strong></font>, AI: <font color=blue><strong>O</strong></font>");
        else if (AIgame && game.getPlayer1().getPlayerSymbol().equals("O"))
            playersSymbols.setText("<html>Player: <font color=blue><strong>O</strong></font>, AI: <font color=red><strong>X</strong></font>");
        else if (!AIgame && game.getPlayer1().getPlayerSymbol().equals("X"))
            playersSymbols.setText("<html>Player 1: <font color=red><strong>X</strong></font>, Player 2: <font color=blue><strong>O</strong></font>");
        else
            playersSymbols.setText("<html>Player 1: <font color=red><strong>O</strong></font>, Player 2: <font color=blue><strong>X</strong></font>");
        playersSymbols.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        playersSymbols.setOpaque(true);
        symbolsPanel.add(playersSymbols);


        // whose turn
        JPanel currentTurnPanel = new JPanel();
        if (game.getPlayer1().isFirst())
            currentTurn = "Player 1";
        else if (!AIgame)
            currentTurn = "Player 2";
        else
            currentTurn = "AI";
        currentTurnDisplay = new JLabel(currentTurn + "'s turn");
        currentTurnPanel.add(currentTurnDisplay);

        JPanel topPanel = new JPanel(new BorderLayout());
        topPanel.add(symbolsPanel, BorderLayout.NORTH);
        topPanel.add(currentTurnPanel, BorderLayout.CENTER);


        // grid buttons
        JPanel gridPanel = new JPanel();
        gridPanel.setLayout(new GridLayout(game.getBoardSize(), game.getBoardSize(), 10, 10));

        gridButtons = new JButton[game.getBoardSize() * game.getBoardSize()];
        for(int i = 0; i < gridButtons.length; i++) {
            gridButtons[i] = new JButton();
            gridButtons[i].setBackground(Color.WHITE);
            gridButtons[i].setFocusable(false);
            gridButtons[i].setFont(new Font("Arial", Font.BOLD, 95 - (game.getBoardSize() - 3) * 5));
            gridPanel.add(gridButtons[i]);
        }
        gridPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));


        // scores
        JPanel scorePanel = new JPanel();
        score = new JLabel();
        score.setFont(new Font("Arial", Font.PLAIN, 20 + (game.getBoardSize() - 3) * 2));
        int player2score = 0;
        int player1score = 0;
        if (AIgame)
            score.setText("Player: " + player1score + ", AI: " + player2score);
        else
            score.setText("Player 1: " + player1score + ", Player 2: " + player2score);
        score.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        score.setOpaque(true);
        scorePanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        scorePanel.add(score);


        frame.add(topPanel,BorderLayout.NORTH);
        frame.add(gridPanel,BorderLayout.CENTER);
        frame.add(scorePanel,BorderLayout.SOUTH);
        frame.setVisible(true);
    }

    public JButton getGridButton(int index) {
        return gridButtons[index];
    }

    public void setCurrentTurn(String currentTurn) {
        this.currentTurn = currentTurn;
        this.currentTurnDisplay.setText(currentTurn + "'s turn");
    }

    public void changeTurn() {
        if (Objects.equals(currentTurn, "Player 2") || Objects.equals(currentTurn, "AI"))
            setCurrentTurn("Player 1");
        else if (Objects.equals(currentTurn, "Player 1") && AIgame)
            setCurrentTurn("AI");
        else
            setCurrentTurn("Player 2");
    }

    public void updateScoreboard(int player1score, int player2score) {
        if (AIgame)
            score.setText("Player: " + player1score + ", AI: " + player2score);
        else
            score.setText("Player 1: " + player1score + ", Player 2: " + player2score);
    }
}

