package org.example;

import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.util.*;
import java.util.List;

@Setter
@Getter
public class Board {
    private int boardSize;
    private final List<List<String>> grid;

    Board(int boardSize) {
        this.boardSize = boardSize;
        if (boardSize > 2 && boardSize < 11) {
            grid = new ArrayList<>(Board.this.boardSize);
            for (int i = 0; i < Board.this.boardSize; i++) {
                grid.add(new ArrayList<>(Board.this.boardSize));
                for (int j = 0; j < Board.this.boardSize; j++)
                    grid.get(i).add("-");
            }
        }
        else
            throw new IllegalArgumentException("Board size should be in range [3, 10]");
    }

    public List<Point> getPossibleMoves() {
        List<Point> possibleMovesList = new ArrayList<>();
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (Objects.equals(grid.get(i).get(j), "-"))
                    possibleMovesList.add(new Point(i, j));
            }
        }
        return possibleMovesList;
    }

    public boolean isBoardFull() {
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++)
                if (grid.get(i).get(j).equals("-"))
                    return false;
        }
        return true;
    }

    public void addSymbolToGrid(String playerSymbol, int posX, int posY) {
        if (posX < this.boardSize && 0 <= posX && posY < this.boardSize && 0 <= posY)
            this.grid.get(posX).set(posY, playerSymbol);
        else if (posX < 0 || posY < 0)
            throw new ArrayIndexOutOfBoundsException("AddSymbolToGrid: grid index cannot be a negative number");
        else
            throw new ArrayIndexOutOfBoundsException("AddSymbolToGrid: grid index is out of bounds");
    }
}
