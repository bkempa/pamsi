package org.example;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Player {
    private String playerSymbol;
    private int points = 0;
    private boolean first;
    private boolean ai;

    Player(String playerSymbol, boolean first, boolean ai) {
        this.playerSymbol = playerSymbol;
        this.first = first;
        this.ai = ai;
    }

    Player(Player player) {
        this.playerSymbol = player.getPlayerSymbol();
        this.points = player.getPoints();
        this.first = player.isFirst();
        this.ai = player.isAi();
    }

    public void addPoint() {
        this.points++;
    }
}
