package org.example;

import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.util.*;
import java.util.List;

@Setter
@Getter
public class Game extends Board {
    private final Player player1;
    private final Player player2;
    private final int symbolsToWin;
    private final int draw = 0;
    private final int win = 1000;
    private final int loss = -1000;
    private final int startDepth = 0;
    private static int score;
    private final boolean AIgame;
    private Player currentTurnPlayer;

    Game(int boardSize, Player p1, Player p2, int symbolsToWin, boolean AIgame) {
        super(boardSize);
        this.AIgame = AIgame;
        player1 = new Player(p1);
        player2 = new Player(p2);
        this.symbolsToWin = symbolsToWin;
        if (p1.isFirst())
            currentTurnPlayer = player1;
        else
            currentTurnPlayer = player2;
    }

    public boolean ifGameIsWon(String playerSymbol) {
        if (Objects.equals(playerSymbol, this.player1.getPlayerSymbol()) || Objects.equals(playerSymbol, this.player2.getPlayerSymbol())) {
            boolean gameWon = false;
            int counter = 0;
            //horizontal
            for (int i = 0; i < getBoardSize(); i++) {
                for (int j = 0; j < getBoardSize(); j++) {
                    if (Objects.equals(getGrid().get(i).get(j), playerSymbol))
                        counter++;
                    else
                        counter = 0;

                    if (counter == symbolsToWin)
                        return true;
                }
                counter = 0;
            }
            //vertical
            for (int i = 0; i < getBoardSize(); i++) {
                for (int j = 0; j < getBoardSize(); j++) {
                    if (Objects.equals(getGrid().get(j).get(i), playerSymbol))
                        counter++;
                    else
                        counter = 0;

                    if (counter == symbolsToWin)
                        return true;
                }
                counter = 0;
            }

            //1st diagonal (0, 0); (1, 1) etc.
            for (int i = 0; i < getBoardSize(); i++) {
                if (Objects.equals(getGrid().get(i).get(i), playerSymbol))
                    counter++;
                else
                    counter = 0;

                if (counter == symbolsToWin)
                    return true;
            }

            counter = 0;
            //2nd diagonal (0, 2); (1, 1) etc.
            for (int i = 0; i < getBoardSize(); i++) {
                if (Objects.equals(getGrid().get(i).get(getBoardSize() - 1 - i), playerSymbol))
                    counter++;
                else
                    counter = 0;

                if (counter == symbolsToWin)
                    return true;
            }

            //smaller diagonals
            int i, j;
            int tmp = 1;
            counter = 0;
            for (int m = 0; m < getBoardSize() - symbolsToWin; m++) {
                i = 0;
                for (j = tmp; j < getBoardSize() - symbolsToWin + 1; j -= (symbolsToWin - 1)) {
                    for (int k = 0; k < symbolsToWin; ++k) {
                        if (Objects.equals(getGrid().get(i).get(j), playerSymbol))
                            counter++;
                        else
                            counter = 0;
                        if (counter == symbolsToWin)
                            return true;
                        ++j;
                        ++i;
                    }
                    i -= (symbolsToWin - 1);
                    counter = 0;
                }
                ++tmp;
            }

            tmp = 1;
            for (int m = 0; m < getBoardSize() - symbolsToWin; ++m) {
                i = 0;
                for (j = tmp; j < getBoardSize() - symbolsToWin + 1; j -= (symbolsToWin - 1)) {
                    for (int k = 0; k < symbolsToWin; ++k) {
                        if (Objects.equals(getGrid().get(j).get(i), playerSymbol))
                            counter++;
                        else
                            counter = 0;
                        if (counter == symbolsToWin)
                            return true;
                        ++j;
                        ++i;
                    }
                    i -= (symbolsToWin - 1);
                    counter = 0;
                }
                ++tmp;
            }

            tmp = getBoardSize() - 2;
            for (int m = 0; m < getBoardSize() - symbolsToWin; ++m) {
                j = 0;
                for (i = tmp; i > symbolsToWin - 2; i += (symbolsToWin - 1)) {
                    for (int k = 0; k < symbolsToWin; ++k) {
                        if (Objects.equals(getGrid().get(i).get(j), playerSymbol))
                            counter++;
                        else
                            counter = 0;
                        if (counter == symbolsToWin)
                            return true;
                        ++j;
                        --i;
                    }
                    j -= (symbolsToWin - 1);
                    counter = 0;
                }
                --tmp;
            }

            tmp = 1;
            for (int m = 0; m < getBoardSize() - symbolsToWin; ++m) {
                i = getBoardSize() - 1;
                for (j = tmp; j < getBoardSize() - symbolsToWin + 1; j -= (symbolsToWin - 1)) {
                    for (int k = 0; k < symbolsToWin; ++k) {
                        if (Objects.equals(getGrid().get(i).get(j), playerSymbol))
                            counter++;
                        else
                            counter = 0;
                        if (counter == symbolsToWin)
                            return true;
                        ++j;
                        --i;
                    }
                    i += (symbolsToWin - 1);
                    counter = 0;
                }
                ++tmp;
            }
            return gameWon;
        }
        else
            throw new IllegalArgumentException("IfGameIsWon: symbol provided as argument is not used in this game by any player");
    }

    public boolean ifGameIsOver() {
        return isBoardFull() || getGameState(player1.getPlayerSymbol()) != draw;
    }

    public int getGameState(String playerSymbol) {
        if (Objects.equals(playerSymbol, this.player1.getPlayerSymbol()) || Objects.equals(playerSymbol, this.player2.getPlayerSymbol())) {
            String opponentSymbol = getOpponentSymbol(playerSymbol);
            if (ifGameIsWon(playerSymbol))
                return win;
            else if (ifGameIsWon(opponentSymbol))
                return loss;
            else
                return draw;
        }
        else
            throw new IllegalArgumentException("GetGameState: symbol provided as argument is not used in this game by any player");
    }

    public String getOpponentSymbol(String playerSymbol) {
        if (Objects.equals(playerSymbol, this.player1.getPlayerSymbol()) || Objects.equals(playerSymbol, this.player2.getPlayerSymbol())) {
            if (Objects.equals(playerSymbol, player1.getPlayerSymbol()))
                return player2.getPlayerSymbol();
            else
                return player1.getPlayerSymbol();
        }
        else
            throw new IllegalArgumentException("GetOpponentSymbol: symbol provided as argument is not used in this game by any player");
    }

    public void clearBoard() {
        for (int i = 0; i < getBoardSize(); i++)
            for (int j = 0; j < getBoardSize(); j++)
                addSymbolToGrid("-", i, j);
    }

    public Point aiMove() {
        Map.Entry<Integer, Point> aiMove = new AbstractMap.SimpleEntry<>(minmax(player2.getPlayerSymbol(), startDepth, loss, win));
        if(aiMove.getValue().getX() != -1 && aiMove.getValue().getY() != -1)
            addSymbolToGrid(player2.getPlayerSymbol(), (int) aiMove.getValue().getX(), (int) aiMove.getValue().getY());
        return aiMove.getValue();
    }

    public Map.Entry<Integer, Point> minmax(String symbol, int depth, int alpha, int beta) {
        Point bestMove = new Point(-1, -1);
        int bestScore = (Objects.equals(symbol, player2.getPlayerSymbol())) ? loss : win;

        if (isBoardFull() || getGameState(player2.getPlayerSymbol()) != draw) {
            bestScore = getGameState(player2.getPlayerSymbol());
            return new AbstractMap.SimpleEntry<>(bestScore, bestMove);
        }

        List<Point> legalMoves = getPossibleMoves();

        for (Point currentMove : legalMoves) {
            addSymbolToGrid(symbol, (int) currentMove.getX(), (int) currentMove.getY());
            int depthLimit = 10;
            //Adjusting the tree's depth to reduce waiting for AI's turn time according to the chosen board size
            if (getBoardSize() > 3)
                depthLimit = 5;
            //Maximizing "max" player's turn
            if (Objects.equals(symbol, player2.getPlayerSymbol())) {
                if (depth < depthLimit)
                    score = minmax(player1.getPlayerSymbol(), depth + 1, alpha, beta).getKey();

                if (bestScore < score) {
                    bestScore = score - depth * 10;
                    bestMove = currentMove;
                    //Alpha-beta pruning
                    alpha = Math.max(alpha, bestScore);
                    addSymbolToGrid("-", (int) currentMove.getX(), (int) currentMove.getY());
                    if (beta <= alpha)
                        break;
                }
            } //Minimizing "min" player's turn
            else {
                if (depth < depthLimit)
                    score = minmax(player2.getPlayerSymbol(), depth + 1, alpha, beta).getKey();

                if (bestScore > score) {
                    bestScore = score + depth * 10;
                    bestMove = currentMove;
                    //Alpha-beta pruning
                    beta = Math.min(beta, bestScore);
                    addSymbolToGrid("-", (int) currentMove.getX(), (int) currentMove.getY());
                    if (beta <= alpha)
                        break;
                }
            }
            addSymbolToGrid("-", (int) currentMove.getX(), (int) currentMove.getY()); //Undo the move
        }
        return new AbstractMap.SimpleEntry<>(bestScore, bestMove);
    }

    public void changeTurnPlayer() {
        if (currentTurnPlayer == player1)
            this.currentTurnPlayer = player2;
        else
            this.currentTurnPlayer = player1;
    }
}

