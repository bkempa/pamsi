package org.example;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;

@Getter
@Setter
public class GameOverGUI {

    private final JFrame frame;
    private final JLabel winner;
    private final JLabel score;
    private final JButton[] menu;
    private final boolean AIgame;

    GameOverGUI(boolean AIgame) {
        this.AIgame = AIgame;
        frame = new JFrame("Tic-Tac-Toe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ImageIcon icon = new ImageIcon("src/main/resources/tic-tac-toe-icon.png");
        frame.setIconImage(icon.getImage());
        frame.setPreferredSize(new Dimension(280, 250));
        frame.setResizable(false);


        // who won
        JPanel winnerPanel = new JPanel();
        winner = new JLabel();
        winner.setFont(new Font("Arial", Font.PLAIN, 20));
        winner.setOpaque(true);
        winnerPanel.add(winner);

        // scores
        JPanel scorePanel = new JPanel();
        score = new JLabel();
        score.setFont(new Font("Arial", Font.PLAIN, 20));
        score.setOpaque(true);
        scorePanel.add(score);


        // menu buttons
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.Y_AXIS));
        menu = new JButton[3];
        String[] menuButtonsText = new String[]{"Play again", "Change game mode", "Exit"};
        for(int i = 0; i < menu.length; i++) {
            menu[i] = new JButton();
            menu[i].setBackground(new Color(0xe2e0e2));
            menu[i].setFocusable(false);
            menu[i].setFont(new Font("Arial", Font.PLAIN, 20));
            menu[i].setText(menuButtonsText[i]);
            menu[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            menu[i].setMaximumSize(new Dimension(450, 100));
            menuPanel.add(menu[i]);
        }
        menuPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));


        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.add(winnerPanel);
        frame.add(scorePanel);
        frame.add(menuPanel);
        frame.pack();
        frame.setVisible(true);
    }

    public JButton getMenuButton(int index) {
        return menu[index];
    }
    public void updateScoreboard(int player1score, int player2score) {
        if (AIgame)
            this.score.setText("<html>Player 1: <strong>" + player1score + "</strong>, &ensp AI: <strong>" + player2score + "</strong>");
        else
            this.score.setText("<html>Player 1: <strong>" + player1score + "</strong>, &ensp Player 2: <strong>" + player2score + "</strong>");
    }
}

