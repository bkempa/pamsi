package org.example;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;
import java.util.stream.IntStream;

public class Controller implements ActionListener {
    private Game game;
    private final StartGameGUI startGameGUI;
    private BoardGUI boardGUI;
    private GameOverGUI gameOverGUI;
    Controller() {
        this.startGameGUI = new StartGameGUI();
        startGameGUI.getConfirmSize().addActionListener(this);
        startGameGUI.getSizesBox().addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        // start GUI - start the game
        if (e.getSource() == startGameGUI.getConfirmSize()) {
            createGame();

            // AI game and player does not start
            if (!game.getPlayer1().isFirst() && game.isAIgame()) {
                makeAImove();
                changeTurn();
                // enabling grid buttons on available spaces
                addActionListenerToAvailableSpaces();
            }
            // enabling all grid buttons
            else
                IntStream.range(0, boardGUI.getGridButtons().length).forEach(i -> boardGUI.getGridButton(i).addActionListener(this));
        }
        // adjust start game GUI to chosen board size (eg. if size changed to 5 -> 3, 4 and 5 symbols in a row to win available)
        else if (e.getSource() == startGameGUI.getSizesBox()) {
            startGameGUI.adjustSymbolsToWinToBoardSize();
            startGameGUI.getSymbolsToWin().setModel(startGameGUI.getSymbolsToWinModel());
        }
        // game over GUI - play again
        else if (e.getSource() == gameOverGUI.getMenuButton(0)) {
            playAgain();
        }
        // game over GUI - change mode
        else if (e.getSource() == gameOverGUI.getMenuButton(1)) {
            changeGameMode();
        }
        // game over GUI - exit
        else if (e.getSource() == gameOverGUI.getMenuButton(2)) {
            exitGame();
        }
        // board GUI - grid buttons
        else {
            for (int i = 0; i < game.getBoardSize() * game.getBoardSize(); i++) {
                if (e.getSource() == boardGUI.getGridButton(i)) {
                    playerMove(i);

                    if (!game.ifGameIsOver())
                        changeTurn();

                    // AI move
                    if (game.isAIgame()) {
                        makeAImove();
                        if (!game.ifGameIsOver()) {
                            changeTurn();
                            addActionListenerToAvailableSpaces();
                        }
                    }

                    if (game.ifGameIsOver()) {
                        int player1state = game.getGameState(game.getPlayer1().getPlayerSymbol());
                        if (player1state == game.getWin())
                            game.getPlayer1().addPoint();
                        else if (player1state == game.getLoss())
                            game.getPlayer2().addPoint();

                        boardGUI.updateScoreboard(game.getPlayer1().getPoints(), game.getPlayer2().getPoints());
                        gameOverGUI.getFrame().setVisible(true);
                        this.boardGUI.getFrame().setEnabled(false);
                        winnerDisplay(player1state);
                        gameOverGUI.updateScoreboard(game.getPlayer1().getPoints(), game.getPlayer2().getPoints());
                        gameOverGUI.getFrame().setVisible(true);
                    }
                }
            }
        }
    }

    public void makeAImove() {
        IntStream.range(0, boardGUI.getGridButtons().length).forEach(j -> boardGUI.getGridButton(j).removeActionListener(this));
        Point move = game.aiMove();
        if (move.x != -1 && move.y != -1) {
            game.addSymbolToGrid(game.getPlayer2().getPlayerSymbol(), move.x, move.y);
            if (Objects.equals(game.getPlayer2().getPlayerSymbol(), "X"))
                boardGUI.getGridButton(move.x * game.getBoardSize() + move.y).setForeground(Color.RED);
            else
                boardGUI.getGridButton(move.x * game.getBoardSize() + move.y).setForeground(Color.BLUE);
            boardGUI.getGridButton(move.x * game.getBoardSize() + move.y).setText(game.getPlayer2().getPlayerSymbol());
        }
    }

    public void createGame() {
        startGameGUI.getFrame().dispose();
        Player player1 = new Player((String) startGameGUI.getSymbolsBox().getSelectedItem(), startGameGUI.getPlayer1starts().isSelected(), startGameGUI.getChooseAI().isSelected());
        Player player2;
        if (Objects.equals(startGameGUI.getSymbolsBox().getSelectedItem(), "X"))
            player2 = new Player("O", startGameGUI.getPlayer1starts().isSelected(), startGameGUI.getChooseAI().isSelected());
        else
            player2 = new Player("X", startGameGUI.getPlayer1starts().isSelected(), startGameGUI.getChooseAI().isSelected());

        this.game = new Game(Integer.parseInt((String) Objects.requireNonNull(startGameGUI.getSizesBox().getSelectedItem())), player1, player2,  Integer.parseInt((String) Objects.requireNonNull(startGameGUI.getSymbolsToWin().getSelectedItem())), startGameGUI.getChooseAI().isSelected());
        this.boardGUI = new BoardGUI(this.game);
        this.gameOverGUI = new GameOverGUI(startGameGUI.getChooseAI().isSelected());
        IntStream.range(0, gameOverGUI.getMenu().length).forEach(i -> gameOverGUI.getMenuButton(i).addActionListener(this));
        gameOverGUI.getFrame().setVisible(false);
    }

    public void changeTurn() {
        game.changeTurnPlayer();
        boardGUI.changeTurn();
    }

    public void addActionListenerToAvailableSpaces() {
        for (int j = 0; j < game.getBoardSize(); j++)
            for (int k = 0; k < game.getBoardSize(); k++)
                if (Objects.equals(game.getGrid().get(j).get(k), "-"))
                    boardGUI.getGridButton(game.getBoardSize() * j + k).addActionListener(this);
    }

    public void playerMove(int index) {
        if (game.getCurrentTurnPlayer().getPlayerSymbol().equals("X")) {
            boardGUI.getGridButton(index).setForeground(Color.RED);
            boardGUI.getGridButton(index).setText("X");
        }
        else {
            boardGUI.getGridButton(index).setForeground(Color.BLUE);
            boardGUI.getGridButton(index).setText("O");
        }
        boardGUI.getGridButton(index).removeActionListener(this);
        game.addSymbolToGrid(game.getCurrentTurnPlayer().getPlayerSymbol(), index / game.getBoardSize(), index % game.getBoardSize());
    }

    public void changeGameMode() {
        gameOverGUI.getFrame().dispose();
        boardGUI.getFrame().dispose();
        gameOverGUI.getFrame().dispose();
        SwingUtilities.invokeLater(Controller::new);
    }

    public void playAgain() {
        if (game.getPlayer1().isFirst())
            game.setCurrentTurnPlayer(game.getPlayer1());
        else
            game.setCurrentTurnPlayer(game.getPlayer2());
        game.clearBoard();
        boardGUI.getFrame().dispose();
        gameOverGUI.getFrame().dispose();
        this.boardGUI = new BoardGUI(this.game);
        IntStream.range(0, boardGUI.getGridButtons().length).forEach(i -> boardGUI.getGridButton(i).addActionListener(this));
        boardGUI.updateScoreboard(game.getPlayer1().getPoints(), game.getPlayer2().getPoints());
    }

    public void exitGame() {
        boardGUI.getFrame().dispose();
        gameOverGUI.getFrame().dispose();
    }

    public void winnerDisplay(int player1state) {
        if (game.isAIgame()) {
            if (player1state == game.getLoss())
                gameOverGUI.getWinner().setText("You lost");
            else if (player1state == game.getWin())
                gameOverGUI.getWinner().setText("You won!");
            else
                gameOverGUI.getWinner().setText("Draw");
        }
        else {
            if (player1state == game.getLoss())
                gameOverGUI.getWinner().setText("Player 2 won!");
            else if (player1state == game.getWin())
                gameOverGUI.getWinner().setText("Player 1 won!");
            else
                gameOverGUI.getWinner().setText("Draw");
        }
    }
}
