package org.example;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    @Test
    void getPossibleMoves() {
        Board board = new Board(3);
        board.addSymbolToGrid("X", 0, 0);
        board.addSymbolToGrid("O", 1, 1);
        board.addSymbolToGrid("X", 2, 2);
        List<Point> expectedResult = new ArrayList<>();
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                if (i != j)
                    expectedResult.add(new Point(i, j));
        assertEquals(expectedResult, board.getPossibleMoves());
    }

    @Test
    void isBoardFull() {
        Board board = new Board(3);
        board.addSymbolToGrid("X", 0, 0);
        board.addSymbolToGrid("O", 0, 1);
        board.addSymbolToGrid("X", 0, 2);
        assertFalse(board.isBoardFull());
        for (int i = 1; i < 3; i++)
            for (int j = 0; j < 3; j++)
                board.addSymbolToGrid("X", i, j);
        assertTrue(board.isBoardFull());
    }

    @Test
    void addSymbolToGrid() {
        Board board = new Board(5);
        board.addSymbolToGrid("X", 4, 2);
        board.addSymbolToGrid("O", 1, 3);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (i == 4 && j == 2)
                    assertEquals("X", board.getGrid().get(i).get(j));
                else if (i == 1 && j == 3)
                    assertEquals("O", board.getGrid().get(i).get(j));
                else
                    assertEquals("-", board.getGrid().get(i).get(j));
            }
        }
    }

    @Test
    void addSymbolToGridNegativeIndex() {
        Board board = new Board(5);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> board.addSymbolToGrid("X", -1, 0));
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> board.addSymbolToGrid("X", 0, -1));
    }

    @Test
    void addSymbolToGridIndexOutOfBounds() {
        Board board = new Board(5);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> board.addSymbolToGrid("X", 5, 0));
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> board.addSymbolToGrid("X", 0, 5));
    }
}