package org.example;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    @Test
    void ifGameIsWonVertical() {
        /*
             -------------------
            | - | X | - | - | - |
            | - | O | - | - | - |
            | - | X | - | - | - |
            | - | X | - | - | - |
            | - | X | - | - | - |
             -------------------
         */
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 0, 1);
        game.addSymbolToGrid("O", 1, 1);
        game.addSymbolToGrid("X", 2, 1);
        game.addSymbolToGrid("X", 3, 1);
        assertFalse(game.ifGameIsWon("X"));
        game.addSymbolToGrid("X", 4, 1);
        assertTrue(game.ifGameIsWon("X"));
    }

    @Test
    void ifGameIsWonHorizontal() {
        /*
             -------------------
            | - | - | - | - | - |
            | - | - | - | - | - |
            | - | - | - | - | - |
            | X | O | X | X | X |
            | - | - | - | - | - |
             -------------------
         */
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 3, 0);
        game.addSymbolToGrid("O", 3, 1);
        game.addSymbolToGrid("X", 3, 2);
        game.addSymbolToGrid("X", 3, 3);
        assertFalse(game.ifGameIsWon("X"));
        game.addSymbolToGrid("X", 3, 4);
        assertTrue(game.ifGameIsWon("X"));
    }

    @Test
    void ifGameIsWonMainDiagonal() {
        /*
             -------------------
            | X | - | - | - | - |
            | - | O | - | - | - |
            | - | - | X | - | - |
            | - | - | - | X | - |
            | - | - | - | - | X |
             -------------------
         */
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 0, 0);
        game.addSymbolToGrid("O", 1, 1);
        game.addSymbolToGrid("X", 2, 2);
        game.addSymbolToGrid("X", 3, 3);
        assertFalse(game.ifGameIsWon("X"));
        game.addSymbolToGrid("X", 4, 4);
        assertTrue(game.ifGameIsWon("X"));
    }

    @Test
    void ifGameIsWonDiagonalFromLeftBottomCorner() {
        /*
             -------------------
            | - | - | - | - | X |
            | - | - | - | X | - |
            | - | - | X | - | - |
            | - | O | - | - | - |
            | X | - | - | - | - |
             -------------------
         */
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 4, 0);
        game.addSymbolToGrid("O", 3, 1);
        game.addSymbolToGrid("X", 2, 2);
        game.addSymbolToGrid("X", 1, 3);
        assertFalse(game.ifGameIsWon("X"));
        game.addSymbolToGrid("X", 0, 4);
        assertTrue(game.ifGameIsWon("X"));
    }

    @Test
    void ifGameIsWonSmallerDiagonalRightToTheMainDiagonal() {
        /*
             -------------------
            | - | - | X | - | - |
            | - | - | - | X | - |
            | - | - | - | - | X |
            | - | - | - | - | - |
            | - | - | - | - | - |
             -------------------
         */
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 0, 2);
        game.addSymbolToGrid("X", 1, 3);
        assertFalse(game.ifGameIsWon("X"));
        game.addSymbolToGrid("X", 2, 4);
        assertTrue(game.ifGameIsWon("X"));
    }

    @Test
    void ifGameIsWonSmallerDiagonalLeftToTheMainDiagonal() {
        /*
             -------------------
            | - | - | - | - | - |
            | - | - | - | - | - |
            | - | X | - | - | - |
            | - | - | X | - | - |
            | - | - | - | X | - |
             -------------------
         */
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 2, 1);
        game.addSymbolToGrid("X", 3, 2);
        assertFalse(game.ifGameIsWon("X"));
        game.addSymbolToGrid("X", 4, 3);
        assertTrue(game.ifGameIsWon("X"));
    }

    @Test
    void ifGameIsWonSmallerDiagonalRightToTheDiagonalFromLeftBottomCorner() {
        /*
             -------------------
            | - | - | - | - | - |
            | - | - | - | - | - |
            | - | - | - | - | X |
            | - | - | - | X | - |
            | - | - | X | - | - |
             -------------------
         */
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 2, 4);
        game.addSymbolToGrid("X", 3, 3);
        assertFalse(game.ifGameIsWon("X"));
        game.addSymbolToGrid("X", 4, 2);
        assertTrue(game.ifGameIsWon("X"));
    }

    @Test
    void ifGameIsWonSmallerDiagonalLeftToTheDiagonalFromLeftBottomCorner() {
        /*
             -------------------
            | - | - | - | X | - |
            | - | - | X | - | - |
            | - | X | - | - | - |
            | - | - | - | - | - |
            | - | - | - | - | - |
             -------------------
         */
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 0, 3);
        game.addSymbolToGrid("X", 1, 2);
        assertFalse(game.ifGameIsWon("X"));
        game.addSymbolToGrid("X", 2, 1);
        assertTrue(game.ifGameIsWon("X"));
    }

    @Test
    void ifGameIsWonWrongArgument() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(3, player1, player2, 3, false);
        assertThrows(IllegalArgumentException.class, () -> game.ifGameIsWon("Z"));
    }

    @Test
    void getGameStateWin() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 0, 0);
        game.addSymbolToGrid("X", 1, 1);
        game.addSymbolToGrid("X", 2, 2);
        assertEquals(game.getWin(), game.getGameState("X"));
    }

    @Test
    void getGameStateLoss() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        game.addSymbolToGrid("X", 0, 0);
        game.addSymbolToGrid("X", 1, 1);
        game.addSymbolToGrid("X", 2, 2);
        assertEquals(game.getLoss(), game.getGameState("O"));
    }

    @Test
    void getGameStateDraw() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(5, player1, player2, 3, false);
        assertEquals(game.getDraw(), game.getGameState("X"));
    }

    @Test
    void getGameStateWrongArgument() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(3, player1, player2, 3, false);
        assertThrows(IllegalArgumentException.class, () -> game.getGameState("Z"));
    }

    @Test
    void getOpponentSymbol() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(3, player1, player2, 3, false);
        assertEquals("O", game.getOpponentSymbol("X"));
        assertEquals("X", game.getOpponentSymbol("O"));
    }

    @Test
    void getOpponentSymbolWrongArgument() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(3, player1, player2, 3, false);
        assertThrows(IllegalArgumentException.class, () -> game.getOpponentSymbol("Z"));
    }

    @Test
    void clearBoard() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(3, player1, player2, 3, false);
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                game.addSymbolToGrid("X", i, j);
        game.clearBoard();
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                assertEquals("-", game.getGrid().get(i).get(j));
    }

    @Test
    void aiMove() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(3, player1, player2, 3, false);
        game.addSymbolToGrid("X", 0, 0);
        game.addSymbolToGrid("X", 1, 1);
        Point move = game.aiMove();
        assertEquals(2, move.getX());
        assertEquals(2, move.getY());
    }

    @Test
    void changeTurnPlayer() {
        Player player1 = new Player("X", true, false);
        Player player2 = new Player("O", false, false);
        Game game = new Game(3, player1, player2, 3, false);
        assertEquals(game.getPlayer1(), game.getCurrentTurnPlayer());
        game.changeTurnPlayer();
        assertEquals(game.getPlayer2(), game.getCurrentTurnPlayer());
    }
}