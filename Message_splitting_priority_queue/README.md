
# Message splitting

Program simulating message sending. In the beginning the user types a message that
is split to a random number of packs. The packs’ order is shuffled. A pack consists
of a message part and its ID that is a priority used in the implemented priority queue
(in a C++ version it’s based on a doubly-linked list, in Java and Python the priority
queue is based on a binary heap). The shuffled packs are inserted to the queue from
which they are removed in a correct order presenting an original message to the
receiver.


## Features

- Priority queue implementations
- Randomizing packs number



## Installation

### C++
To run this program you only need to type these commands in your Linux system terminal:
```bash
  chmod +x Makefile
  make
```

### Java
It is possible to run the program in multiple ways, e.g. by importing it and running it in your IDE or using [Exec Maven plugin](http://www.mojohaus.org/exec-maven-plugin/). To do it you should type in the command line:
```bash
  mvn exec:java -Dexec.mainClass="com.example.Main"
```

### Python
You are able to run the program via your IDE or command line (you need to have [Python](https://www.python.org/downloads/) installed first) using this command:
```bash
  python main.py
```
    
## Screenshots

[![message1.png](https://i.postimg.cc/kXN0vdCv/message1.png)](https://postimg.cc/XpJ1nmpr)

