class PriorityQueue:
    def __init__(self, capacity):
        self.capacity = capacity
        self.size = 0
        self.H = []

    def parent(self, index):
        return (index - 1) // 2

    def left_child(self, index):
        return (2 * index) + 1

    def right_child(self, index):
        return (2 * index) + 2

    def shift_up(self, index):
        while index > 0 and self.H[self.parent(index)] > self.H[index]:
            self.swap(self.parent(index), index)
            index = self.parent(index)

    def shift_down(self, index):
        max_index = index
        left = self.left_child(index)
        right = self.right_child(index)

        if left <= self.size and self.H[left] < self.H[max_index]:
            max_index = left

        if right <= self.size and self.H[right] < self.H[max_index]:
            max_index = right

        if index != max_index:
            self.swap(index, max_index)
            self.shift_down(max_index)

    def insert(self, p):
        self.H.append(p)
        self.shift_up(self.size)
        self.size += 1

    def extract_min(self):
        result = self.H[0]
        self.H[0] = self.H[self.size - 1]
        self.size -= 1
        self.shift_down(0)
        return result

    def get_min(self):
        return self.H[0]

    def swap(self, i, j):
        self.H[i], self.H[j] = self.H[j], self.H[i]
