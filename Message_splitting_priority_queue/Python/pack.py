import random
from functools import total_ordering


class Pack:
    def __init__(self, message_part, pack_id):
        self.message_part = message_part
        self.id = pack_id

    @staticmethod
    def convert_to_packs(message, number_of_packs):
        current_id = 1
        packs = []
        if number_of_packs == 1:
            packs.append(Pack(message, number_of_packs))
        else:
            pack_length = len(message) // number_of_packs
            for i in range(number_of_packs):
                if i < number_of_packs - 3:
                    message_part = message[:pack_length]
                    message = message[pack_length:]
                elif i == number_of_packs - 3 or i == number_of_packs - 2:
                    temp_pack_length = len(message) // (number_of_packs - i)
                    message_part = message[:temp_pack_length]
                    message = message[temp_pack_length:]
                else:
                    message_part = message

                packs.append(Pack(message_part, current_id))
                current_id += 1

        random.shuffle(packs)
        return packs

    @total_ordering
    def __lt__(self, other):
        return self.id < other.id

    @total_ordering
    def __eq__(self, other):
        return self.id == other.id

    @total_ordering
    def __gt__(self, other):
        return self.id > other.id
