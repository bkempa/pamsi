import random
import pack
import priority_queue


message = input("Enter message:\n")
number_of_packs = random.randint(1, len(message))

packs = pack.Pack.convert_to_packs(message, number_of_packs)
print("Split packs")
for pack in packs:
    print("[" + pack.message_part + ",\tID: " + str(pack.id) + "]")

packs_queue = priority_queue.PriorityQueue(number_of_packs)
for pack in packs:
    packs_queue.insert(pack)

print("Recreated message:")
while packs_queue.size > 0:
    print(packs_queue.extract_min().message_part, end="")
