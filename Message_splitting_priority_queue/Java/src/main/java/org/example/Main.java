package org.example;

import java.util.Random;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        System.out.println("Enter message:");
        Scanner scanner = new Scanner(System.in);
        String message = scanner.nextLine();
        final int randomPackNumber = randomizePackNumber(message.length());
        Pack[] messagePacks = Pack.returnShuffledPacks(message, randomPackNumber);
        System.out.println("\nSplit packs:");
        for(Pack i : messagePacks)
            System.out.println(i);

        PriorityQueue packPriorityQueue = new PriorityQueue(randomPackNumber);
        for (Pack i : messagePacks)
            packPriorityQueue.insert(i);

        System.out.println("\nReconstructed message:");
        while (packPriorityQueue.getSize() > 0)
            System.out.print(packPriorityQueue.extractMin().getPart());
    }

    public static int randomizePackNumber(int messageLength) {
        Random random = new Random();
        return random.nextInt(messageLength) + 1;
    }
}