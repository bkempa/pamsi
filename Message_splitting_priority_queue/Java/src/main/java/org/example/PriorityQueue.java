package org.example;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PriorityQueue {
    private final Pack[] H;
    private int size;

    PriorityQueue(int capacity) {
        this.size = 0;
        H = new Pack[capacity];
    }

    int parent(int i) {
        if (i < this.H.length && i >= 0)
            return (i - 1) / 2;
        else if (i >= this.H.length)
            throw new IllegalArgumentException("Parent method: index exceeds queue capacity");
        else
            throw new IllegalArgumentException("Parent method: index cannot be less than 0");
    }

    int leftChild(int i) {
        if (i < this.H.length && i >= 0)
            return ((2 * i) + 1);
        else if (i >= this.H.length)
            throw new IllegalArgumentException("LeftChild method: index exceeds queue capacity");
        else
            throw new IllegalArgumentException("LeftChild method: index cannot be less than 0");
    }

    int rightChild(int i) {
        if (i < this.H.length && i >= 0)
            return ((2 * i) + 2);
        else if (i >= this.H.length)
            throw new IllegalArgumentException("RightChild method: index exceeds queue capacity");
        else
            throw new IllegalArgumentException("RightChild method: index cannot be less than 0");
    }

    void shiftUp(int i) {
        if (i >= this.H.length)
            throw new IllegalArgumentException("ShiftUp method: index exceeds queue capacity");
        else if (i < 0)
            throw new IllegalArgumentException("ShiftUp method: index cannot be less than 0");
        else {
            while (i > 0 && H[parent(i)].compareTo(H[i]) > 0) {
                swap(parent(i), i);
                i = parent(i);
            }
        }
    }

    void shiftDown(int i) {
        if (i >= this.H.length)
            throw new IllegalArgumentException("ShiftDown method: index exceeds queue capacity");
        else if (i < 0)
            throw new IllegalArgumentException("ShiftDown method: index cannot be less than 0");
        else {
            int maxIndex = i;
            int l = leftChild(i);
            int r = rightChild(i);

            if (l <= size && H[l].compareTo(H[maxIndex]) < 0)
                maxIndex = l;

            if (r <= size && H[r].compareTo(H[maxIndex]) < 0)
                maxIndex = r;

            if (i != maxIndex) {
                swap(i, maxIndex);
                shiftDown(maxIndex);
            }
        }
    }

    void insert(Pack p) {
        if (this.size < this.H.length) {
            H[size] = p;
            shiftUp(size);
            size++;
        }
        else
            throw new ArrayIndexOutOfBoundsException("Cannot insert element to the queue - it's full");
    }

    Pack extractMin() {
        Pack result = H[0];
        H[0] = H[size - 1];
        size--;
        shiftDown(0);
        return result;
    }

    void changePriority(int i, int priority) {
        if (i >= this.H.length)
            throw new IllegalArgumentException("ChangePriority: index exceeds queue capacity");
        else if (i < 0)
            throw new IllegalArgumentException("ChangePriority: index cannot be less than 0");
        else {
            int oldPriority = H[i].getID();
            H[i].setID(priority);

            if (priority < oldPriority)
                shiftUp(i);
            else
                shiftDown(i);
        }
    }

    Pack getMin() {
        return H[0];
    }

    void remove(int i) {
        if (i >= this.H.length)
            throw new IllegalArgumentException("Remove method: index exceeds queue capacity");
        else if (i < 0)
            throw new IllegalArgumentException("Remove method: index cannot be less than 0");
        else {
            H[i].setID(getMin().getID() + 1);
            shiftUp(i);
            extractMin();
        }
    }

    void swap(int i, int j) {
        if (i >= this.H.length || j >= this.H.length)
            throw new IllegalArgumentException("Swap method: index exceeds queue capacity");
        else if (i < 0 || j < 0)
            throw new IllegalArgumentException("Swap method: index cannot be less than 0");
        else {
            Pack temp = H[i];
            H[i] = H[j];
            H[j] = temp;
        }
    }

}