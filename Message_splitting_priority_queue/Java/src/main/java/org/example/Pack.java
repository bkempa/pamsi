package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Pack implements Comparable<Pack> {
    private String part;
    private int ID;

    public static Pack[] convertToPacks(String message, int numberOfPacks){
        int currentID = 1;
        if (numberOfPacks > 0 && numberOfPacks <= message.length()) {
            Pack[] packs = new Pack[numberOfPacks];
            if (numberOfPacks == 1)
                packs[0] = new Pack(message, currentID);
            else {
                final int packLength = message.length() / numberOfPacks;
                String messagePart;
                for (int i = 0; i < numberOfPacks; i++) {
                    if ((i != numberOfPacks - 1) && (i != numberOfPacks - 2) && (i != numberOfPacks - 3)) {
                        messagePart = message.substring(0, packLength);
                        message = message.substring(packLength);
                    } else if (i == numberOfPacks - 3) {
                        int tempPackLength = message.length() / 3;
                        messagePart = message.substring(0, tempPackLength);
                        message = message.substring(tempPackLength);
                    } else if (i == numberOfPacks - 2) {
                        int tempPackLength = message.length() / 2;
                        messagePart = message.substring(0, tempPackLength);
                        message = message.substring(tempPackLength);
                    } else
                        messagePart = message;

                    packs[i] = new Pack(messagePart, currentID);
                    currentID++;
                }
            }
            return packs;
        }
        else if (numberOfPacks > message.length())
            throw new IllegalArgumentException("Number of packs cannot be greater than message length");
        else
            throw new IllegalArgumentException("Number of packs cannot be less than 1");
    }

    public static Pack[] returnShuffledPacks(String message, int numberOfPacks) {
        return shufflePacks(convertToPacks(message, numberOfPacks));
    }

    public static Pack[] shufflePacks(Pack[] packs) {
        List<Pack> packsList = Arrays.asList(packs);
        Collections.shuffle(packsList);
        packsList.toArray(packs);
        return packs;
    }

    @Override
    public int compareTo(Pack pck) {
        return Integer.compare(this.getID(), pck.getID());
    }

    @Override
    public String toString() {
        return "[" + this.getPart() + ", ID: " + this.getID() + "]";
    }
}
