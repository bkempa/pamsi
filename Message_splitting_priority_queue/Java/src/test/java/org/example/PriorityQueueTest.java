package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PriorityQueueTest {

    @Test
    void parentCorrectIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertEquals(1, queue.parent(4));
    }

    @Test
    void parentTooHighIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.parent(5));
    }

    @Test
    void parentNegativeIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.parent(-1));
    }

    @Test
    void leftChildCorrectIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertEquals(3, queue.leftChild(1));
    }

    @Test
    void leftChildTooHighIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.leftChild(5));
    }

    @Test
    void leftChildNegativeIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.leftChild(-1));
    }

    @Test
    void rightChildCorrectIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertEquals(4, queue.rightChild(1));
    }

    @Test
    void rightChildTooHighIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.rightChild(5));
    }

    @Test
    void rightChildNegativeIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.rightChild(-1));
    }

    @Test
    void shiftUpCorrectIndex() {
        PriorityQueue queue = new PriorityQueue(3);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        queue.getH()[0] = test3;
        queue.getH()[1] = test2;
        queue.getH()[2] = test1;
        queue.setSize(3);
        queue.shiftUp(2);
        assertEquals(queue.getH()[0].getID(), test1.getID());
    }

    @Test
    void shiftUpTooHighIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.shiftUp(5));
    }

    @Test
    void shiftUpNegativeIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.shiftUp(-1));
    }

    @Test
    void shiftDownCorrectIndex() {
        PriorityQueue queue = new PriorityQueue(3);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        queue.getH()[0] = test3;
        queue.getH()[1] = test2;
        queue.getH()[2] = test1;
        queue.setSize(3);
        queue.shiftDown(0);
        assertEquals(queue.getH()[0].getID(), test1.getID());
    }

    @Test
    void shiftDownTooHighIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.shiftDown(5));
    }

    @Test
    void shiftDownNegativeIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.shiftDown(-1));
    }

    @Test
    void insert() {
        PriorityQueue queue = new PriorityQueue(3);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        queue.insert(test3);
        queue.insert(test2);
        queue.insert(test1);
        assertEquals(queue.getH()[0].getID(), test1.getID());
    }

    @Test
    void insertTooManyElements() {
        PriorityQueue queue = new PriorityQueue(2);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        queue.insert(test3);
        queue.insert(test2);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> queue.insert(test1));
    }

    @Test
    void extractMin() {
        PriorityQueue queue = new PriorityQueue(3);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        queue.insert(test3);
        queue.insert(test2);
        queue.insert(test1);
        assertEquals(test1.getID(), queue.extractMin().getID());
    }

    @Test
    void changePriority() {
        PriorityQueue queue = new PriorityQueue(3);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        queue.insert(test3);
        queue.insert(test2);
        queue.insert(test1);
        queue.changePriority(0, 4);
        assertEquals(2, queue.extractMin().getID());
    }

    @Test
    void changePriorityTooHighIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.changePriority(5, 5));
    }

    @Test
    void changePriorityNegativeIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.changePriority(-1, 5));
    }

    @Test
    void getMin() {
        PriorityQueue queue = new PriorityQueue(3);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        queue.insert(test3);
        queue.insert(test2);
        queue.insert(test1);
        assertEquals(1, queue.getMin().getID());
    }

    @Test
    void remove() {
        PriorityQueue queue = new PriorityQueue(5);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        Pack test4 = new Pack("Test4", 4);
        Pack test5 = new Pack("Test5", 5);
        queue.insert(test5);
        queue.insert(test4);
        queue.insert(test3);
        queue.insert(test2);
        queue.insert(test1);
        queue.remove(1);
        assertEquals(4, queue.getSize());
        assertEquals(3, queue.getH()[1].getID());
    }

    @Test
    void removeTooHighIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.remove(5));
    }

    @Test
    void removeNegativeIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.remove(-1));
    }

    @Test
    void swap() {
        PriorityQueue queue = new PriorityQueue(3);
        Pack test1 = new Pack("Test1", 1);
        Pack test2 = new Pack("Test2", 2);
        Pack test3 = new Pack("Test3", 3);
        queue.insert(test3);
        queue.insert(test2);
        queue.insert(test1);
        queue.swap(0, 1);
        assertEquals(3, queue.getMin().getID());
    }

    @Test
    void swapTooHighIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.swap(3, 6));
    }

    @Test
    void swapNegativeIndex() {
        PriorityQueue queue = new PriorityQueue(5);
        assertThrows(IllegalArgumentException.class, () -> queue.swap(-1, 3));
    }

}