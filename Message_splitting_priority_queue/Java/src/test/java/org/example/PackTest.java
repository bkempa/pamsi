package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PackTest {

    @Test
    void messageToOnePack() {
        Pack[] pack = Pack.convertToPacks("Test", 1);
        assertEquals("Test", pack[0].getPart());
        assertEquals(1, pack[0].getID());
    }

    @Test
    void messageToTwoPacks() {
        Pack[] pack = Pack.convertToPacks("Test", 2);
        assertEquals("Te", pack[0].getPart());
        assertEquals(1, pack[0].getID());
        assertEquals("st", pack[1].getPart());
        assertEquals(2, pack[1].getID());
    }

    @Test
    void messageToThreePacks() {
        Pack[] packs = Pack.convertToPacks("Test", 3);
        assertEquals("T", packs[0].getPart());
        assertEquals("e", packs[1].getPart());
        assertEquals("st", packs[2].getPart());
        assertEquals(1, packs[0].getID());
        assertEquals(2, packs[1].getID());
        assertEquals(3, packs[2].getID());
    }

    @Test
    void longMessageToTestShuffling() {
        String message = "Long message to test if shuffling works correctly (message is so long to minimize likeliness to stay on pack's original position)";
        Pack[] packs = Pack.convertToPacks(message, message.length());
        assertEquals("L", packs[0].getPart());
    }

    @Test
    void allMessagePartsHaveEqualLength() {
        String message = "Test message";
        Pack[] packs = Pack.convertToPacks(message, message.length() / 2);
        for (Pack pack : packs) {
            assertEquals(2, pack.getPart().length());
        }
    }

    @Test
    void zeroPacksNumber() {
        assertThrows(IllegalArgumentException.class, () -> Pack.convertToPacks("Test", 0));
    }

    @Test
    void tooBigPacksNumber() {
        assertThrows(IllegalArgumentException.class, () -> Pack.convertToPacks("Test", 5));
    }
}