#include "priorityqueue.hh"
#include <iostream>

void priority_queue::insert(pack *node){
    if (front == nullptr){  //jeśli kolejka jest pusta
        front = node;
        rear = node;
    }
    else if (front->get_id() >= node->get_id()){   //dodanie paczki na początek kolejki
        node->set_next(front);
        front->set_prev(node);
        front = node;
    }
    else if (rear->get_id() <= node->get_id()){    //dodanie paczki na koniec kolejki
        node->set_prev(rear);
		rear->set_next(node);
		rear = node;
    }
	else{
		pack *temp = front;
		while (temp->get_id() < node->get_id()) //szukanie odpowiedniego miejsca do wstawienia danej paczki
			temp = temp->get_next();
		//Wstawianie paczki w znalezione miejsce
		node->set_next(temp);
		node->set_prev(temp->get_prev());
		temp->set_prev(node);
		if (node->get_prev() != nullptr)
			node->get_prev()->set_next(node);
	}
	size++;
}

bool priority_queue::is_empty(){
    if (size == 0)
        return true;
    else
        return false;
}

pack* priority_queue::peek(){
    if (is_empty()==true){
        std::cout << "\nKolejka jest pusta\n";
        return nullptr;
    }
    else
        return front;
}

pack* priority_queue::remove_min(){
    if(is_empty()==true){
        std::cout << "Kolejka jest pusta\n";
        return nullptr;
    }
    else{
        pack* removed_pack = this->peek();
        if(size == 1)
            rear = front = nullptr;
        else{
            front = front->get_next();
            front->set_prev(nullptr);
        }
        size--;
        return removed_pack;
    }
}

void priority_queue::print_queue(){
	pack *node = front;
	while (node != nullptr){
		std::cout << "" << node->get_str();
		node = node->get_next();
	}
	std::cout << "\n";
}