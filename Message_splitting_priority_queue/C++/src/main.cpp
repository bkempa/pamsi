#include <iostream>
#include <string>
//#include <chrono>
#include "pack.hh"
#include "priorityqueue.hh"


int main(){ 
    //driver
    std::cout << "\tSymulacja zadanej sytuacji i demonstracja dzialania programu\n";
    std::cout << "\t\t\t(komputer nadawcy)\n";
    std::cout << "\t\tWpisz wiadomosc, ktora chcesz wyslac:\n";
    std::string message;
    std::getline(std::cin, message);
    while(message.length()==0){
        std::cout << "Wiadomosc nie moze byc pusta\n";
        std::getline(std::cin, message);
    }
    std::cout << "\n\t\tPodzial wpisanej wiadomosci na paczki\n";
    priority_queue q;
    const int pck_num = random_pack_num(message.length());
    pack *tab = new pack[pck_num];
    pack* sent_collection = convert_to_pack(message, tab, pck_num);
    //auto start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < pck_num; ++i){
        std::cout << "Tab[" << i << "]: " << sent_collection[i].get_str() << "\n";
        q.insert(&sent_collection[i]);
    }
    //auto finish = std::chrono::high_resolution_clock::now();
    //std::chrono::duration<double, std::milli> ms_double = finish - start;
    //std::cout << "Czas potrzebny na dodanie do kolejki " << pck_num << " paczek: " << ms_double.count() << " ms.\n";
    std::cout << "\n Wyswietlanie odtworzonej wiadomosci \n";
    while(!q.is_empty())
        std::cout << q.remove_min()->get_str();
    std::cout << "\n";
    delete[] tab;
}