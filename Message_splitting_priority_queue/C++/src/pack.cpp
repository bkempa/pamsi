#include "pack.hh"
#include <random>
#include <string>
#include <iostream>
#include <iterator>
#include <algorithm>

pack* convert_to_pack(std::string msg, pack* collection, int num_of_pack){
    unsigned int id_number = 1;
    if(num_of_pack==1){
        collection[0] = pack(id_number, msg);
        return collection;
    }
    else{
        const int pack_length = msg.length()/num_of_pack;   //podzial wiadomosci, kazda paczka ma podobna liczbe znakow
        for(int i = 0; i < num_of_pack; ++i){
            if (i != num_of_pack-1 && i!= num_of_pack-2){
                std::string msg_part = msg.substr(0, pack_length);
                msg.erase(0, pack_length);                
                collection[i] = pack(id_number, msg_part);
            }
            else{    //ostatnia dwie paczki zawiera pozostala czesc wiadomosci podzielona na 2
                if(i == num_of_pack - 2){
                    int tmp_pack_length = msg.length()/2;
                    std::string msg_part = msg.substr(0, tmp_pack_length);
                    msg.erase(0, tmp_pack_length);
                    collection[i] = pack(id_number, msg_part);                   
                }
                else{
                    std::string msg_part = msg.substr(0, msg.length());
                    msg.erase(msg.begin(), msg.end());
                    collection[i] = pack(id_number, msg_part);
                }               
            }          
            id_number++;
            std::cout << "Paczka " << i+1 << ": " << collection[i].get_str() << "\n";
        }
        std::random_shuffle(&collection[0], &collection[num_of_pack]);    //tablica polosowana, teraz dodac do struktury
        std::cout << "Kolejnosc paczek w tablicy zostala pomieszana - symulacja odebrania paczek w losowej kolejnosci.\n";
        std::cout << "Aktualna kolejnosc paczek (wyswietlono identyfikatory (klucze) danej paczki, ktore byly przyznawane kolejno od 1 oraz odpowiadajace im fragmenty wiadomosci):\n";
        for(int i = 0; i < num_of_pack; ++i)
            std::cout << "Tab[" << i << "]\t=" << "\tID: " << collection[i].get_id() << ",\ttekst: " << collection[i].get_str() << "\n";
        return collection;
    }
}

int random_pack_num(int message_length){  //losowanie liczby paczek, na ktora zostanie podzielona wiadomosc
    unsigned int number_of_packs;
    if(message_length==1)
        number_of_packs = 1;
    else{
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist(2, message_length);
        number_of_packs = dist(rng);
    }
    std::cout << "Wylosowana liczba paczek, na ktora podzielono wiadomosc: " << number_of_packs << "\n";
    return number_of_packs;
}