#ifndef PRIORITYQUEUE_HH
#define PRIORITYQUEUE_HH

#include "pack.hh"

class priority_queue {
    pack *front;
    pack *rear;
    unsigned int size;
    public:
        priority_queue(){front = rear = nullptr; size = 0;};
        void insert(pack *node);
        bool is_empty();
        pack* peek();
        int get_size(){return size;};
        pack* remove_min();
        void print_queue();
};

#endif
