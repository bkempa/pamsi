#ifndef PACK_HH
#define PACK_HH

#include <string>

class pack {
    unsigned int id;
    std::string str;
    pack *prev;
    pack *next;
    public:
        pack(){};
        pack(const unsigned int num, const std::string symb){id = num; str = symb; next = prev = nullptr;};
        unsigned int get_id() const {return id;};
        std::string get_str() const {return str;};
        pack* get_prev() const {return prev;};
        void set_prev(pack *act_prev) {prev = act_prev;};
        pack* get_next() const {return next;};
        void set_next(pack *act_next) {next = act_next;};
};

pack* convert_to_pack(std::string msg, pack* collection, int num_of_pack);
int random_pack_num(int message_length);

#endif